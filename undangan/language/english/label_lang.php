<?php
    $lang['full_name'] = "Full Name";
    $lang['short_name'] = "Short Name";
    $lang['gender'] = "Gender";
    $lang['phone_number'] = "Phone Number";
    $lang['address'] = "Address";
    $lang['description'] = "Description";
    $lang['picture'] = "Picture";
    $lang['degree'] = "Degree";
    $lang['change_picture'] = "Change Picture";
    $lang['location'] = "Location";
    $lang['event_date'] = "Date";
    $lang['event_time'] = "Time";
    $lang['marital_status'] = "Marital Status";
    $lang['married'] = "Married";
    $lang['single'] = "Single";

    $lang['update'] = "Update";
    $lang['new'] = "New";
    $lang['delete'] = "Delete";
    $lang['detail'] = "Detail";
    $lang['save'] = "Save";
    $lang['back'] = "Back";

    $lang['warning_title'] = "Warning";
    $lang['warning_content'] = "Are you sure to delete this data ? ";
    $lang['cancel'] = "Cancel";
    $lang['yes'] = "Yes";
    $lang['male'] = "Male";
    $lang['famale'] = "Famale";
    $lang['label_title'] = "Title";
    $lang['image'] = "Image";
    $lang['change_image'] = "Change Image";
    $lang['attend'] = "Attend";
    $lang['message'] = "Message";
    $lang['link'] = "Link";
    $lang['settings'] = "Settings";
    $lang['bride'] = "Bride";
    $lang['groom'] = "Groom";
    $lang['event'] = "Event";
    $lang['guests_count'] = "Number of guests";
    $lang['people'] = 'People';
    $lang['will_attend'] = "Will Attend";
    $lang['maybe_attend'] = "Maybe Attend";
    $lang['not_attend'] = "Not Attend";
    $lang['invitation_read_count'] = "Invitation Read";
?>