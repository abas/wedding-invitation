<?php
    $lang['full_name'] = "Nama Lengkap";
    $lang['short_name'] = "Nama Panggilan";
    $lang['gender'] = "Jenis Kelamin";
    $lang['phone_number'] = "Nomer Hp";
    $lang['address'] = "Alamat";
    $lang['description'] = "Deskripsi";
    $lang['picture'] = "Gambar";
    $lang['degree'] = "Gelar";
    $lang['change_picture'] = "Ganti Gambar";
    $lang['location'] = "Lokasi";
    $lang['event_date'] = "Tanggal";
    $lang['event_time'] = "Waktu";
    $lang['marital_status'] = "Status Perkawinan";
    $lang['married'] = "Menikah";
    $lang['single'] = "Lajang";

    $lang['update'] = "Ubah";
    $lang['new'] = "Tambah";
    $lang['delete'] = "Hapus";
    $lang['detail'] = "Selengkapnya";
    $lang['save'] = "Simpan";
    $lang['back'] = "Kembali";

    $lang['warning_title'] = "Peringatan";
    $lang['warning_content'] = "Apakah anda yakin ingin menghapus data ini ? ";
    $lang['cancel'] = "Batal";
    $lang['yes'] = "Ya";
    $lang['male'] = "Laki - Laki";
    $lang['famale'] = "Perempuan";
    $lang['label_title'] = "Judul";
    $lang['image'] = "Gambar";
    $lang['change_image'] = "Ganti Gambar";
    $lang['attend'] = "Hadir";
    $lang['message'] = "Nasihat / Doa";
    $lang['link'] = "Url";
    $lang['settings'] = "Pengaturan";
    $lang['bride'] = "Mempelai Wanita";
    $lang['groom'] = "Mempelai Pria";
    $lang['event'] = "Acara";
    $lang['guests_count'] = "Jumlah Tamu";
    $lang['people'] = 'Orang';
    $lang['will_attend'] = "Akan Hadir";
    $lang['maybe_attend'] = "Mungkin Hadir";
    $lang['not_attend'] = "Tidak Hadir";
    $lang['invitation_read_count'] = "Undangan Dibaca";
?>