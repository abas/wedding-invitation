<?php

if (!function_exists('bs_header')) {

    function bs_header() {
        
    }

}

if (!function_exists('bs_confirm')) {

    function bs_confirm($data = array()) {
        $html = 'data-toggle="modal" ';
        $html .= 'data-target="#modalConfirm" ';
        $html .= 'data-pesan = "' . $data['pesan'] . '" ';
        $html .= 'data-url = "' . $data['url'] . '"';
        $html .= 'data-judul = "' . $data['judul'] . '" ';
        return $html;
    }

}
if (!function_exists('bs_confirm_content')) {

    function bs_confirm_content($animation = 'fade', $bgcolor = 'bg-warning') {
        $ci =& get_instance();
        $html = '<div class="modal ' . $animation . '" id="modalConfirm" tabindex="-1" role="dialog" aria-labelledby="modalConfirmLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content ' . $bgcolor . '">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel"></h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body bg-light">
                    </div>
                    <div class="modal-footer p-2">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">'.$ci->lang->line('cancel').'</button>
                        <a class="btn btn-primary" id="url">'.$ci->lang->line('yes').'</a>
                    </div>
                </div>
            </div>
        </div>';
        return $html;
    }

}

if (!function_exists('bs_confirm_js')) {

    function bs_confirm_js() {
        $html = '<script>';
        $html .= "$('#modalConfirm').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget)
                var pesan = button.data('pesan')
                var judul = button.data('judul')
                var url = button.data('url')
                var modal = $(this)
                modal.find('.modal-header').html(judul)
                modal.find('.modal-body').html(pesan)
                modal.find('.modal-footer a').attr('href', url)
            })";
        $html .= '</script>';
        return $html;
    }

}
if (!function_exists('bs_info')) {

    function bs_info($data = array()) {
        $html = 'data-toggle="modal" ';
        $html .= 'data-target="#modalinfo" ';
        $html .= 'data-pesan = "' . $data['pesan'] . '" ';
        $html .= 'data-judul = "' . $data['judul'] . '" ';
        return $html;
    }

}
if (!function_exists('bs_info_content')) {

    function bs_info_content($animation = 'fade', $bgcolor = 'bg-warning') {
        $html = '<div class="modal ' . $animation . '" id="modalinfo" tabindex="-1" role="dialog" aria-labelledby="modalinfoLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content ' . $bgcolor . '">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel"></h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body bg-light">
                    </div>
                    <div class="modal-footer p-2">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">OK</button>
                    </div>
                </div>
            </div>
        </div>';
        return $html;
    }

}

if (!function_exists('bs_info_js')) {

    function bs_info_js() {
        $html = '<script>';
        $html .= "$('#modalinfo').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget)
                var pesan = button.data('pesan')
                var judul = button.data('judul')
                var modal = $(this)
                modal.find('.modal-header').html(judul)
                modal.find('.modal-body').html(pesan)
            })";
        $html .= '</script>';
        return $html;
    }

}

if (!function_exists('bs_card')) {

    function bs_card($card_data = array(), $col = 'md 6', $extra = 'card-primary my-card mt-4') {
        $col_split = explode(' ', $col);
        $card = '<div class="col-' . $col_split[0] . '-' . $col_split[1] . '">';
        $card .= '<div class="card ' . $extra . '">';
        if (isset($card_data['header'])) {
            $card .= $card_data['header'];
        }
        if (isset($card_data['body'])) {
            $card .= $card_data['body'];
        }
        if (isset($card_data['footer'])) {
            $card .= $card_data['footer'];
        }
        if (isset($card_data['content'])) {
            $card .= $card_data['content'];
        }
        $card .= '</div>';
        $card .= '</div>';
        return $card;
    }

}

if (!function_exists('card_header')) {

    function card_header($title = 'Card Title', $extra_class = '_mt-2 bg-dark-green-light',$extra_button='') {
        $card_header = '<div class="card-header my-card-header ' . $extra_class . '">';
        $card_header .= '<span class="card-title my-card-title">' . $title . '</span> '.$extra_button.'</div>';
        return $card_header;
    }

}

if (!function_exists('card_body')) {

    function card_body($content = '', $class = '') {
        $card_body = '<div class="card-body ' . $class . '">';
        $card_body .= $content;
        $card_body .= '</div>';
        return $card_body;
    }

}
if (!function_exists('card_footer')) {

    function card_footer($content = '') {
        $card_footer = '<div class="card-footer">';
        $card_footer .= $content;
        $card_footer .= '</div>';
        return $card_footer;
    }

}

if (!function_exists('bs_table')) {

    function bs_table($data = array()) {
        $table = '<div class="table-responsive">';
        $table .= '<table class="table table-striped table-bordered table-sm" cellspacing="0" width="100%" id="myTable" style="font-size: 10pt;">';
        if (isset($data['thead'])) {
            $table .= $data['thead'];
        }
        if (isset($data['tbody'])) {
            $table .= $data['tbody'];
        }
        if (isset($data['tfooter'])) {
            $table .= $data['tfooter'];
        }
        $table .= '</table>';
        $table .= '</div>';
        return $table;
    }

}

if (!function_exists('table_head')) {

    function table_head($data = array(), $class = 'table-inverse') {
        $thead = '<thead class="' . $class . '">';
        if (is_array($data)) {
            $thead .= '<tr>';
            $thead .= '<th style="width: 2%;">No</th>';
            foreach ($data as $key => $val) {
                $thead .= '<th>' . $val . '</th>';
            }
            $thead .= '</tr>';
        }
        $thead .= '</thead>';
        return $thead;
    }

}

if (!function_exists('table_body')) {

    function table_body($tr_data) {
        $tbody = '<tbody>';
        $tbody .= $tr_data;
        $tbody .= '</tbody>';
        return $tbody;
    }

}

if (!function_exists('bs_alert')) {

    function bs_alert($content = '', $class = 'alert-primary') {
        $alert = '<div class="alert ' . $class . '" role="alert">';
        $alert .= $content;
        $alert .= DIV_END;
        return $alert;
    }

}

if (!function_exists('bs_form_group')) {

    function bs_form_group($data = array()) {
        $fg = '';
        if (is_array($data)) {
            foreach ($data as $key => $value) {
                $fg .= '<div class="form-group">';
                if ($data[$key]['type'] == 'dropdown') {
                    $fg .= form_dropdown($data[$key]['name'], $data[$key]['options'], set_value($data[$key]['name']), 'class="form-control select22" id="' . $data[$key]['name'] . '"' . ($data[$key]['required'] ? 'required=""' : ''));
                } else if ($data[$key]['type'] == 'radio') {
                    $fg .= $data[$key]['content'];
                } else {
                    $fg .= form_input($data[$key]['name'], set_value($data[$key]['name']), 'class="form-control" id="' . $data[$key]['name'] . '" placeholder="' . $data[$key]['placeholder'] . '"' . ($data[$key]['required'] ? 'required=""' : ''), $data[$key]['type']);
                }
                $fg .= DIV_END;
            }
        }
        return $fg;
    }

}

if (!function_exists('bs_form_input')) {

    function bs_form_input($data = '', $value = '', $extra = '', $type = 'text') {

        $bfi = '<div class="form-group">';
        $bfi .= form_input($data, $value, 'class="form-control ' . _bs_class($extra) . '"' . $extra, $type);
        $bfi .= '</div>';
        return $bfi;
    }

}
if (!function_exists('bs_form_dropdown')) {

    function bs_form_dropdown($data = '', $options = array(), $selected = array(), $extra = '') {

        $bfi = '<div class="form-group">';
        $bfi .= form_dropdown($data, $options, $selected, 'class="form-control select22" ' . $extra);
        $bfi .= '</div>';
        return $bfi;
    }

}

if (!function_exists('bs_form_inline')) {

    function bs_form_inline($data = '', $value = '', $checked = FALSE, $extra = '') {
        $fi = '';
        if (is_array($data)) {
            foreach ($data as $key => $val) {
                $fi .= '<div class="form-check form-check-inline">';
                $fi .= '<label class="form-check-label">';

                $fi .= form_radio($data, $value, $checked, $extra);
                $fi .= '</label></div>';
            }
        }
        return $fi;
    }

}

if (!function_exists('_bs_class')) {

    function _bs_class($attributes) {
        $class_extra = '';
        if (empty($attributes)) {
            return '';
        }
        if (is_object($attributes)) {
            $attributes = (array) $attributes;
        }
        if (is_array($attributes)) {
            foreach ($attributes as $key => $value) {
                $class_extra .= ($key == 'class') ? $value : '';
            }
        }
        return $class_extra;
    }

}
if (!function_exists('bs_btn_reset')) {

    function bs_btn_reset($data, $value, $extra) {
        $html = '';
        $html .= form_reset($data, $value, "" . $extra);
    }

}
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

