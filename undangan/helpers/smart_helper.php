<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
if (!function_exists('auto_inc')) {

    function auto_inc($model_name, $pk) {
        $CI = & get_instance();
        $id = $CI->$model_name->get_last_id();
        if (!empty($id)) {
            $idd = $id->$pk + 1;
        } else {
            $idd = '1';
        }
        return $idd;
    }

}

if (!function_exists('uid')) {

    function uid() {
        return random_string('numeric', 10);
    }

}

if (!function_exists('drop_list')) {

    function drop_list($models, $pk, $name, $label, $method = NULL, $param = NULL) {
        $CI = & get_instance();
        $split_name = explode('|', $name);
        if ($method === NULL) {
            $get = $CI->$models->get_all('asc');
            if (!empty($get)) {
                foreach ($get as $val) {
                    $list[''] = $label;
                    if (count($split_name) > 1) {
                        $list[$val->$pk] = '<b>' . $val->$split_name[0] . '</b> - ' . $val->$split_name[1];
                    } else {
                        $list[$val->$pk] = $val->$name;
                    }
                }
            } else {
                $list[''] = $label;
            }
            return $list;
        }

        if ($param === NULL) {
            $get = $CI->$models->$method();
            if (!empty($get)) {
                foreach ($get as $val) {
                    $list[''] = $label;
                    if (count($split_name) > 1) {
                        $list[$val->$pk] = '<b>' . $val->$split_name[0] . '</b> - ' . $val->$split_name[1];
                    } else {
                        $list[$val->$pk] = $val->$name;
                    }
                }
            } else {
                $list[''] = $label;
            }
            return $list;
        }

        $get = $CI->$models->$method($param);
        if (!empty($get)) {
            foreach ($get as $val) {
                $list[''] = $label;
                if (count($split_name) > 1) {
                    $list[$val->$pk] = '<b>' . $val->$split_name[0] . '</b> - ' . $val->$split_name[1];
                } else {
                    $list[$val->$pk] = $val->$name;
                }
            }
        } else {
            $list[''] = $label;
        }
        return $list;
    }

}

if (!function_exists('query_sql')) {

    function query_sql($sql, $type = NULL,$database=NULL) {
        $CI = & get_instance();
        if ($database != NULL){
            $CI->load->database($database,TRUE);
        }
        if ($type === NULL) {
            return $CI->db->query($sql)->result();
        }
        return $CI->db->query($sql)->$type();
    }

}

if (!function_exists('tgl_indo')) {

    function tgl_indo($tgl) {
        $ubah = gmdate($tgl, time() + 60 * 60 * 8);
        $pecah = explode("-", $ubah);
        $tanggal = $pecah[2];
        $bulan = bulan($pecah[1]);
        $tahun = $pecah[0];
        return $tanggal . ' ' . $bulan . ' ' . $tahun;
    }

}

if (!function_exists('bulan')) {

    function bulan($bln) {
        switch ($bln) {
            case 1:
                return "Januari";
                break;
            case 2:
                return "Februari";
                break;
            case 3:
                return "Maret";
                break;
            case 4:
                return "April";
                break;
            case 5:
                return "Mei";
                break;
            case 6:
                return "Juni";
                break;
            case 7:
                return "Juli";
                break;
            case 8:
                return "Agustus";
                break;
            case 9:
                return "September";
                break;
            case 10:
                return "Oktober";
                break;
            case 11:
                return "November";
                break;
            case 12:
                return "Desember";
                break;
        }
    }

}

if (!function_exists('nama_hari')) {

    function nama_hari($tanggal) {
        $ubah = gmdate($tanggal, time() + 60 * 60 * 8);
        $pecah = explode("-", $ubah);
        $tgl = $pecah[2];
        $bln = $pecah[1];
        $thn = $pecah[0];

        $nama = date("l", mktime(0, 0, 0, $bln, $tgl, $thn));
        $nama_hari = "";
        if ($nama == "Sunday") {
            $nama_hari = "Minggu";
        } else if ($nama == "Monday") {
            $nama_hari = "Senin";
        } else if ($nama == "Tuesday") {
            $nama_hari = "Selasa";
        } else if ($nama == "Wednesday") {
            $nama_hari = "Rabu";
        } else if ($nama == "Thursday") {
            $nama_hari = "Kamis";
        } else if ($nama == "Friday") {
            $nama_hari = "Jumat";
        } else if ($nama == "Saturday") {
            $nama_hari = "Sabtu";
        }
        return $nama_hari;
    }

}
if (!function_exists('hitung_mundur')) {

    function hitung_mundur($wkt) {
        $waktu = array(365 * 24 * 60 * 60 => "tahun",
            30 * 24 * 60 * 60 => "bulan",
            7 * 24 * 60 * 60 => "minggu",
            24 * 60 * 60 => "hari",
            60 * 60 => "jam",
            60 => "menit",
            1 => "detik");
        $hitung = strtotime(gmdate("Y-m-d H:i:s", time() + 60 * 60 * 8)) - $wkt;
        $hasil = array();
        if ($hitung < 5) {
            $hasil = 'kurang dari 5 detik yang lalu';
        } else {
            $stop = 0;
            foreach ($waktu as $periode => $satuan) {
                if ($stop >= 6 || ($stop > 0 && $periode < 60))
                    break;
                $bagi = floor($hitung / $periode);
                if ($bagi > 0) {
                    $hasil[] = $bagi . ' ' . $satuan;
                    $hitung -= $bagi * $periode;
                    $stop++;
                } else if ($stop > 0)
                    $stop++;
            }
            $hasil = implode(' ', $hasil) . ' yang lalu';
        }
        return $hasil;
    }

}

if (!function_exists('hitung_umur')){
    function hitung_umur($tanggal_lahir){
        $biday = new DateTime($tanggal_lahir);
        $today = new DateTime();
        $diff = $today->diff($biday);
        return $diff->y.' Tahun' ;
    }
}

if (!function_exists('selisih_waktu')) {

    function selisih_waktu($mulai, $selesai, $komen) {
        list($h, $m, $s) = explode(":", $mulai);
        $dtAwal = mktime($h, $m, $s, "1", "1", "1");
        list($h, $m, $s) = explode(":", $selesai);
        $dtAkhir = mktime($h, $m, $s, "1", "1", "1");
        $dtSelisih = $dtAkhir - $dtAwal;

        $totalmenit = $dtSelisih / 60;
        $jam = explode(".", $totalmenit / 60);
        $sisamenit = ($totalmenit / 60) - $jam[0];
        $sisamenit2 = $sisamenit * 60;
        $selisih = round($sisamenit2, 0);
        if ($jam[0] <= 0) {
            $rjam = '';
        } else {
            $rjam = $jam[0] . " jam";
        }
        if ($selisih <= 0) {
            $rme = '';
        } else {
            $rme = $selisih . " menit";
        }
        if ($rjam === '' and $rme === '') {
            $result = 'Sesuai Jadwal';
        } else {
            $result = $komen . " " . $rjam . " " . $rme;
        }
        return $result;
    }

}

if (!function_exists('curPageURL')) {

    function curPageURL() {
        $pageURL = 'http';
        if (isset($_SERVER["HTTPS"]) and $_SERVER["HTTPS"] == "on") {
            $pageURL .= "s";
        }
        $pageURL .= "://";

        if (isset($_SERVER["SERVER_PORT"]) and $_SERVER["SERVER_PORT"] != "80") {
            $pageURL .= $_SERVER["SERVER_NAME"] . ":" . $_SERVER["SERVER_PORT"] . $_SERVER["REQUEST_URI"];
        } else {
            $pageURL .= $_SERVER["SERVER_NAME"] . $_SERVER["REQUEST_URI"];
        }

        return $pageURL;
    }

}

if (!function_exists('sharethis')) {

    function sharethis($platform = 'facebook,twitter', $url = '') {

        $render = '';
        $platform = explode(',', $platform);

        foreach ($platform as $m) {
            if ($m == 'facebook') {
                $render .= '<a href="http://facebook.com/share.php?u=' . $url . '" target="_blank" title="Bagikan"><img src="' . base_url() . 'asset/images/facebook.png"></a>&nbsp;';
            }

            if ($m == 'twitter') {
                $render .= '<a href="http://twitter.com/home?status=' . $url . '" target="_blank" title="Bagikan"><img src="' . base_url() . 'asset/images/twitter-icon.png"></a>&nbsp;';
            }
        }

        return $render;
    }

}

if (!function_exists('_render_template')) {

    function _render_template($module, $view_file, $title, $data = null) {
        $data['footer_width'] = 'container-fluid';
        $data['title'] = $title;
        $data['modules'] = $module;
        $data['view_file'] = $view_file;
        $data['menu'] = get_menu('backend');
        return Modules::run('template/_smart', $data);
    }

}

if (!function_exists('_render_frontend')) {

    function _render_frontend($module, $view_file, $title, $data = null) {
        $data['title'] = $title;
        $data['modules'] = $module;
        $data['view_file'] = $view_file;
        return Modules::run('template/frontend', $data);
    }

}

if (!function_exists('style')) {

    function style($param, $extra = '') {
        if ($param == 'table') {
            return 'class="table table-striped table-bordered table-sm" cellspacing="0" width="100%" id="myTable" style="font-size: 10pt;"';
        }
        if ($param == 'popover') {
            return 'data-toggle="popover" data-trigger="hover" data-placement="bottom" data-content="' . $extra . '"';
        }
    }

}

if (!function_exists('set_alert')) {

    function set_alert($pesan = NULL, $url_fallback = '') {
        echo "<script>alert('" . $pesan . "'); window.location=('" . site_url($url_fallback) . "')</script>";
    }

}

if (!function_exists('status')) {

    function status($status) {
        if ($status = 0) {
            return 'Non Aktif';
        }
        return 'Aktif';
    }

}

if (!function_exists('get_btn')) {

    function get_btn($data = array(), $extra_html = '') {
        $CI = & get_instance();
        $html = '<a href="' . $data['url_edit'] . '" ' . style('popover', 'Edit Data') . ' class="' . $data['extra_class'] . '" data=\'' . json_encode($data['json_data']) . '\'><i class="fa fa-edit"></i></a> &nbsp;';
        $html .= '<a href="' . $data['url_hapus'] . '" class="" ' . bs_confirm($data['konfirmasi_hapus']) . ' title="Hapus Data"><i class="fa fa-trash"></i></a> &nbsp;';
        $html .= $extra_html;
        return $html;
    }

}

if (!function_exists('list_bulan')) {

    function list_bulan() {

        foreach (range(01, 12) as $val) {
            $results[sprintf('%02d', $val)] = bulan($val);
        }
        return $results;
    }

}
if (!function_exists('kekata')) {

    function kekata($x) {
        $x = abs($x);
        $angka = array("", "satu", "dua", "tiga", "empat", "lima",
            "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
        $temp = "";
        if ($x < 12) {
            $temp = " " . $angka[$x];
        } else if ($x < 20) {
            $temp = kekata($x - 10) . " belas";
        } else if ($x < 100) {
            $temp = kekata($x / 10) . " puluh" . kekata($x % 10);
        } else if ($x < 200) {
            $temp = " seratus" . kekata($x - 100);
        } else if ($x < 1000) {
            $temp = kekata($x / 100) . " ratus" . kekata($x % 100);
        } else if ($x < 2000) {
            $temp = " seribu" . kekata($x - 1000);
        } else if ($x < 1000000) {
            $temp = kekata($x / 1000) . " ribu" . kekata($x % 1000);
        } else if ($x < 1000000000) {
            $temp = kekata($x / 1000000) . " juta" . kekata($x % 1000000);
        } else if ($x < 1000000000000) {
            $temp = kekata($x / 1000000000) . " milyar" . kekata(fmod($x, 1000000000));
        } else if ($x < 1000000000000000) {
            $temp = kekata($x / 1000000000000) . " trilyun" . kekata(fmod($x, 1000000000000));
        }
        return $temp;
    }

}

if (!function_exists('terbilang')) {

    function terbilang($x, $style = 4) {
        if ($x < 0) {
            $hasil = "minus " . trim(kekata($x));
        } else {
            $hasil = trim(kekata($x));
        }
        switch ($style) {
            case 1:
                $hasil = strtoupper($hasil);
                break;
            case 2:
                $hasil = strtolower($hasil);
                break;
            case 3:
                $hasil = ucwords($hasil);
                break;
            default:
                $hasil = ucfirst($hasil);
                break;
        }
        return $hasil;
    }

}

if (!function_exists('jumlah_hari_per_bulan')) {

    function jumlah_hari_per_bulan($bulan=NULL,$tahun=NULL) {
        if (is_null($bulan)){
            $bulan = date('m');
        }
        if (is_null($tahun)){
            $tahun = date('Y');
        }
        return cal_days_in_month(CAL_GREGORIAN, $bulan, $tahun);
    }

}
if(!function_exists('set_table')){
    function set_table($header,$tabel_id='myTable',$table_class='table table-striped table-bordered table-sm'){
        $CI = & get_instance();
        $tmpl = array(
            'table_open' => '<table class="'.$table_class.'" cellspacing="0" width="100%" id="'.$tabel_id.'" style="font-size: 10pt;">',
            'thead_open' => '<thead class="table-inverse"');
        $CI->table->set_template($tmpl);
        $CI->table->set_heading($header);
    }
}
if(!function_exists('get_active_theme')){
    function get_active_theme(){
        return 'dua';
    }
}