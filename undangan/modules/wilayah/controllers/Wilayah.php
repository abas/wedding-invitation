<?php

defined('BASEPATH') OR EXIT('Bolo maapu juu. jao ijin');

class Wilayah extends MX_Controller {

    public function __construct() {
        parent::__construct();
        if (!$this->auth->sudah_login()) {
            redirect('apps/dashboard', 'refresh');
        }
    }

    public function index() {
       $id = $this->input->post('id');
       $getWilayah = $this->input->post('wilayah');
       $refWilayah = $this->input->post('ref_wilayah');
       $model = 'm_'.$getWilayah;
       $cek = $this->$model->get_many_by(array($refWilayah.'_id'=>$id));
       if (!empty($cek)){
           $result = '<option>Pilih '.ucfirst($getWilayah).'</option>';
           foreach($cek as $val){
            $result .= '<option value="'.$val->id.'">'.$val->nama.'</option>';
           }
       }else{
           $result = '<option value="">Pilih '.uppercase($getWilayah).'</option>';
       }
       echo $result;
    }

}
