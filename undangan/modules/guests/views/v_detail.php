<div class="box">
        <div class="box-header with-border">
            <h3 class="box-title"><?= $title ?></h3>
        </div>
            <div class="box-body">
                    <div class="col-md-6">
                        <dl class="dl-horizontal">
                            <dt><?= $this->lang->line('full_name') ?></dt>
                            <dd><?= $detail->full_name.', '.$detail->degree; ?></dd>
                            <dt><?= $this->lang->line('gender') ?></dt>
                            <dd><?= $detail->gender ?></dd>
                            <dt><?= $this->lang->line('phone_number') ?></dt>
                            <dd><?= $detail->phone_number ?></dd>
                            <dt><?= $this->lang->line('address') ?></dt>
                            <dd><?= $detail->address ?></dd>
                            <dt><?= $this->lang->line('marital_status') ?></dt>
                            <dd><?= $this->lang->line($detail->marital_status) ?></dd>
                            <dt><?= $this->lang->line('link') ?></dt>
                            <dd><?= site_url().'?a='.$detail->token; ?></dd>
                        </dl>
                    </div>
                    <div class="col-md-6">
                        <dl class="dl-horizontal">
                            <dt>Facebook</dt>
                            <dd>https://www.facebook.com/<?= $sosmed->facebook ?></dd>
                            <dt>Instagram</dt>
                            <dd>https://www.instagram.com/<?= $sosmed->instagram ?></dd>
                            <dt>Twitter</dt>
                            <dd>https://twitter.com/<?= $sosmed->twitter ?></dd>
                        </dl>
                    </div>
            </div><!-- /.box-body -->
            <div class="box-footer">
                <a href="#" onclick="location.replace(document.referrer)" class="btn btn-default" data-toggle="tooltip" title="<?= $this->lang->line('back') ?>"><i class="fa fa-backward"></i></a>
                <a href="<?= site_url('apps/guests/'.$detail->id.'/update') ?>" class="btn btn-primary pull-right" data-toggle="tooltip" title="<?= $this->lang->line('update') ?> Data"><i class="fa fa-edit"></i></a>
            </div>
    </div><!-- /.box -->