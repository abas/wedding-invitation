<div class="box">
        <div class="box-header with-border">
            <h3 class="box-title"><?= $title ?></h3>
        </div>
        <?= form_open('','class="form-horizontal" id="formPerson"') ?>
            <div class="box-body">
                <input type="hidden" name="id" value="<?= $edit->id ?>">
                <input type="hidden" name="id_sosmed" value="<?= $sosmed->id ?>">
                    <div class="form-group">
                        <label for="nama" class="control-label col-sm-3"><?= $this->lang->line('full_name') ?></label>
                        <div class="col-sm-5">
                            <?= form_input('nama_lengkap',set_value('nama_lengkap',$edit->full_name),'class="form-control" id="nama" required') ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="gelar" class="control-label col-sm-3"><?= $this->lang->line('degree') ?></label>
                        <div class="col-sm-5">
                            <?= form_input('gelar',set_value('gelar',$edit->degree),'class="form-control" id="gelar"') ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="no_hp" class="control-label col-sm-3"><?= $this->lang->line('phone_number') ?></label>
                        <div class="col-sm-5">
                            <?= form_input('no_hp',set_value('no_hp',$edit->phone_number),'class="form-control" id="no_hp"') ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="jenis_kelamin" class="control-label col-sm-3"><?= $this->lang->line('gender') ?></label>
                        <div class="col-sm-8">
                            <?php if($edit->gender == 'MALE'): ?>
                                <?= form_radio('jenis_kelamin','MALE',TRUE) ?> <?= $this->lang->line('male') ?> &nbsp;
                                <?= form_radio('jenis_kelamin','FAMALE',FALSE) ?> <?= $this->lang->line('famale') ?>
                            <?php else: ?>
                                <?= form_radio('jenis_kelamin','MALE',FALSE) ?> <?= $this->lang->line('male') ?> &nbsp;
                                <?= form_radio('jenis_kelamin','FAMALE',TRUE) ?> <?= $this->lang->line('famale') ?>
                            <?php endif; ?>
                            
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="marital_status" class="control-label col-sm-3"><?= $this->lang->line('marital_status') ?></label>
                        <div class="col-sm-8">
                            <?php if($edit->gender == 'MALE'): ?>
                                <?= form_radio('marital_status','married',TRUE) ?> <?= $this->lang->line('married') ?> &nbsp;
                                <?= form_radio('marital_status','single',FALSE) ?> <?= $this->lang->line('single') ?>
                            <?php else: ?>
                                <?= form_radio('marital_status','married',FALSE) ?> <?= $this->lang->line('married') ?> &nbsp;
                                <?= form_radio('marital_status','single',TRUE) ?> <?= $this->lang->line('single') ?>
                            <?php endif; ?>
                            
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="alamat" class="control-label col-sm-3"><?= $this->lang->line('address') ?></label>
                        <div class="col-sm-5">
                            <textarea name="alamat" id="alamat" cols="30" rows="3" class="form-control"><?= $edit->address ?></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="facebook" class="control-label col-sm-3">Facebook</label>
                        <div class="col-sm-5">
                            <?= form_input('facebook',set_value('facebook',$sosmed->facebook),'class="form-control" id="facebook"') ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="instagram" class="control-label col-sm-3">Instagram</label>
                        <div class="col-sm-5">
                        <?= form_input('instagram',set_value('instagram',$sosmed->instagram),'class="form-control" id="instagram"') ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="twitter" class="control-label col-sm-3">Twitter</label>
                        <div class="col-sm-5">
                        <?= form_input('twitter',set_value('twitter',$sosmed->twitter),'class="form-control" id="twitter"') ?>
                        </div>
                    </div>
            </div><!-- /.box-body -->
            <div class="box-footer">
                <a href="<?= site_url('apps/guests') ?>" class="btn btn-default" data-toggle="tooltip" title="Back"><i class="fa fa-backward"></i></a>
                <button type="submit" class="btn btn-info pull-right">Save Change</button>
            </div>
            <?= form_close(); ?>
    </div><!-- /.box -->