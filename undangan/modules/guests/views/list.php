<div class="box">
        <div class="box-header with-border">
            <h3 class="box-title"><?= $title ?></h3>
            <div class="box-tools pull-right">
                <a href="<?= site_url('apps/guests/new') ?>" class="btn btn-box-tool" data-toggle="tooltip" title="Add New"><i class="fa fa-plus"></i></a>
            </div>
        </div>
        <div class="box-body">
            <div class="table-responsive">
                <?= $table; ?>
            </div>
        </div><!-- /.box-body -->
    </div><!-- /.box -->