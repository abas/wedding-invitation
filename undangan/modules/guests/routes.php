<?php
$route['apps/guests'] = 'guests';
$route['apps/guests/new'] = 'guests/add';
$route['apps/guests/(:any)/delete'] = 'guests/delete/$1';
$route['apps/guests/(:any)/update'] = 'guests/update/$1';
$route['apps/guests/(:any)/detail'] = 'guests/detail/$1';
$route['guests'] = 404;
$route['guests/add'] = 404;
$route['guests/update/$1'] = 404;
$route['guests/detail/$1'] = 404;
$route['guests/delete/$1'] = 404;