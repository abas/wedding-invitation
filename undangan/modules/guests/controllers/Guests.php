<?php

defined('BASEPATH') OR EXIT('Bolo maapu juu. jao ijin');

class Guests extends MX_Controller {

    private $module;
    private $title;

    public function __construct() {
        parent::__construct();
        if (!$this->auth->sudah_login()){
            redirect('apps/login');
        }
        $this->module = 'guests';
        $this->title = 'List';
        $this->load->model('m_guests');
        $this->load->model('m_sosmed');
    }

    public function index() {
        $read_status = $this->input->get('rs');
        $header = array('No', $this->lang->line('full_name'), $this->lang->line('gender'),$this->lang->line('phone_number'),$this->lang->line('address'),$this->lang->line('link'),'#');
        set_table($header);
        if (isset($read_status)){
            $list = $this->m_guests->get_many_by(array('read_status'=>$read_status));
        }else{
            $list = $this->m_guests->get_all();
        }
        if (!empty($list)) {
            $no = 1;
            foreach ($list as $val) {
                $arr_confirm = array(
                    'pesan' => $this->lang->line('warning_content'),
                    'url' => site_url('apps/guests/' . $val->id.'/delete'),
                    'judul' => $this->lang->line('warning_title')
                );
                $token = '<input type="text" style="display:none;" id="copy-me'.$val->id.'" value="'.site_url().'?a='.$val->token.'">';
                $token .= '<div class="tiptop">';
                $token .= '<button class="btn btn-sm btn-warning" id="copy-btn" onclick="copyToClipboard('.$val->id.')">
                <span class="tiptoptext" id="myTiptop'.$val->id.'">Copy to clipboard</span>
                Copy '.$this->lang->line('link').'
                </button>';
                $token .= '</div>';
                $btn = '<a href="' .site_url('apps/guests/'.$val->id.'/update') . '" data-toggle="tooltip" title="'.$this->lang->line('update').'"><i class="fa fa-edit"></i></a> &nbsp;';
                $btn .= '<a href="'.site_url('apps/guests/'.$val->id.'/detail').'" data-toggle="tooltip" title="'.$this->lang->line('detail').'"><i class="fa fa-eye"></i></a> &nbsp;';
                $btn .= '<a href="#" class="" ' . bs_confirm($arr_confirm) . ' data-toggle="tooltip" title="'.$this->lang->line('delete').'"><i class="fa fa-trash"></i></a> &nbsp;';
                $this->table->add_row($no++, $val->full_name.', '.$val->degree, $val->gender, $val->phone_number,$val->address,$token,$btn);
            }
        }
        $data['table'] = $this->table->generate();
        $data['title'] = $this->title;
        $data['modules'] = $this->module;
        $data['view_file'] = 'list';
        echo Modules::run('template', $data);
    }

    public function add(){
        if ($this->input->post()){
            if($this->m_guests->new()){
                $this->m_sosmed->new();
                redirect('apps/guests/new','refresh');
            }else{
                redirect('apps/guests','refresh');
            }
        }else{
            $data['title'] = $this->lang->line('new').' '.$this->lang->line('guests');
            $data['modules'] = $this->module;
            $data['view_file'] = 'v_add';
            echo Modules::run('template', $data);
        }
    }

    public function update($id){
        if ($cek = $this->m_guests->get($id)) {
            if ($this->input->post()){
                $this->m_guests->change();
                $this->m_sosmed->change();
                redirect('apps/guests','refresh');
            }else{
                $data['edit'] = $cek;
                $data['sosmed'] = $this->m_sosmed->get_by(array('guests_id'=>$id));
                $data['title'] = $this->lang->line('update').' Data '.$this->lang->line('guests');
                $data['modules'] = $this->module;
                $data['view_file'] = 'v_update';
                echo Modules::run('template', $data);
            }
        }else{
            redirect('apps/guests','refresh');
        }
    }

    public function detail($id){
        if ($cek = $this->m_guests->get($id)) {
            $data['detail'] = $cek;
            $data['sosmed'] = $this->m_sosmed->get_by(array('guests_id'=>$id));
            $data['title'] = 'Data '.$this->lang->line('guests');
            $data['modules'] = $this->module;
            $data['view_file'] = 'v_detail';
            echo Modules::run('template', $data);
        }else{
            redirect('apps/guests','refresh');
        }
    }

    public function delete($id) {
        if ($cek = $this->m_guests->get($id)) {
            $this->m_sosmed->delete_by(array('guests_id'=>$id));
            $this->m_guests->delete($id);
            redirect('apps/guests');
        } else {
            redirect('apps/dashboard', 'refresh');
        }
    }

}
