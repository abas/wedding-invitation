<div class="box">
        <div class="box-header with-border">
            <h3 class="box-title"><?= $title ?></h3>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <?= form_open_multipart('','class="form-horizontal"',array('prefix'=>'bride','bride_old_pic'=>$bride->picture)); ?>
                    <fieldset class="scheduler-border">
                        <legend class="scheduler-border"><?= $this->lang->line('bride') ?> Information</legend>
                            <div class="form-group">
                                <label for="bride_name" class="control-label col-sm-3"><?= $this->lang->line('full_name') ?></label>
                                <div class="col-sm-9">
                                    <?= form_input('bride_name',set_value('bride_name',$bride->full_name),'class="form-control" id="bride_name" required') ?>
                                    <?= form_hidden('bride_id',$bride->id) ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="bride_short_name" class="control-label col-sm-3"><?= $this->lang->line('short_name') ?></label>
                                <div class="col-sm-9">
                                    <?= form_input('bride_short_name',set_value('bride_short_name',$bride->short_name),'class="form-control" id="bride_short_name"') ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="bride_address" class="control-label col-sm-3"><?= $this->lang->line('address') ?></label>
                                <div class="col-sm-9">
                                    <textarea name="bride_address" id="bride_address" cols="30" rows="3" class="form-control"><?= $bride->address ?></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="bride_description" class="control-label col-sm-3"><?= $this->lang->line('description') ?></label>
                                <div class="col-sm-9">
                                    <textarea name="bride_description" id="bride_description" cols="30" rows="3" class="form-control"><?= $bride->description ?></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="bride_picture" class="control-label col-sm-3"><?= $this->lang->line('picture') ?></label>
                                <div class="col-sm-9">
                                <img src="<?= base_url('uploads/images/'.$bride->picture) ?>" width="100" class="img-responsive">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="bride_picture" class="control-label col-sm-3"><?= $this->lang->line('change_picture') ?></label>
                                <div class="col-sm-9">
                                    <input type="file" name="bride_picture">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="bride_picture" class="control-label col-sm-3">&nbsp;</label>
                                <div class="col-sm-9">
                                <button type="submit" class="btn btn-info pull-right" data-toggle="tooltip" title="<?= $this->lang->line('save') ?>"><i class="fa fa-save"></i></button>
                                </div>
                            </div>
                    </fieldshet>
                    <?= form_close(); ?>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <?= form_open_multipart('','class="form-horizontal"',array('prefix'=>'groom','groom_old_pic'=>$groom->picture)) ?>
                <fieldset class="scheduler-border">
                    <legend class="scheduler-border"><?= $this->lang->line('groom') ?> Information</legend>
                        <div class="form-group">
                            <label for="groom_name" class="control-label col-sm-3"><?= $this->lang->line('full_name') ?></label>
                            <div class="col-sm-9">
                                <?= form_input('groom_name',set_value('groom_name',$groom->full_name),'class="form-control" id="groom_name" required') ?>
                                <?= form_hidden('groom_id',$groom->id) ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="groom_short_name" class="control-label col-sm-3"><?= $this->lang->line('short_name') ?></label>
                            <div class="col-sm-9">
                                <?= form_input('groom_short_name',set_value('groom_short_name',$groom->short_name),'class="form-control" id="groom_short_name"') ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="groom_address" class="control-label col-sm-3"><?= $this->lang->line('address') ?></label>
                            <div class="col-sm-9">
                                <textarea name="groom_address" id="groom_address" cols="30" rows="3" class="form-control"><?= $groom->address ?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="groom_description" class="control-label col-sm-3"><?= $this->lang->line('description') ?></label>
                            <div class="col-sm-9">
                                <textarea name="groom_description" id="groom_description" cols="30" rows="3" class="form-control"><?= $groom->description ?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="groom_picture" class="control-label col-sm-3"><?= $this->lang->line('picture') ?></label>
                            <div class="col-sm-9">
                                <img src="<?= base_url('uploads/images/'.$groom->picture) ?>" width="100" class="img-responsive">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="groom_picture" class="control-label col-sm-3"><?= $this->lang->line('change_picture') ?></label>
                            <div class="col-sm-8">
                                <input type="file" name="groom_picture">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="bride_picture" class="control-label col-sm-3">&nbsp;</label>
                            <div class="col-sm-9">
                            <button type="submit" class="btn btn-info pull-right" data-toggle="tooltip" title="<?= $this->lang->line('save') ?>"><i class="fa fa-save"></i></button>
                            </div>
                        </div>
                    </fieldshet>
                    <?= form_close(); ?>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <?= form_open_multipart('','class="form-horizontal"',array('prefix'=>'groom','groom_old_pic'=>$groom->picture)) ?>
                <fieldset class="scheduler-border">
                    <legend class="scheduler-border"><?= $this->lang->line('event') ?></legend>
                        <div class="form-group">
                            <label for="name" class="control-label col-sm-3"><?= $this->lang->line('label_title') ?></label>
                            <div class="col-sm-9">
                                <?= form_input('name',set_value('name',$event->name),'class="form-control" id="name" required') ?>
                                <?= form_hidden('id',$event->id) ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="location" class="control-label col-sm-3"><?= $this->lang->line('location') ?></label>
                            <div class="col-sm-9">
                                <?= form_input('location',set_value('location',$event->location),'class="form-control" id="location"') ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="date_information" class="control-label col-sm-3"><?= $this->lang->line('event_date') ?></label>
                            <div class="col-sm-9">
                                <?= form_input('date_information',set_value('date_information',$event->date_information),'class="form-control" id="date_information"') ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="time_information" class="control-label col-sm-3"><?= $this->lang->line('event_time') ?></label>
                            <div class="col-sm-9">
                                <?= form_input('time_information',set_value('time_information',$event->time_information),'class="form-control" id="time_information"') ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="bride_picture" class="control-label col-sm-3">&nbsp;</label>
                            <div class="col-sm-9">
                            <button type="submit" class="btn btn-info pull-right" data-toggle="tooltip" title="<?= $this->lang->line('save') ?>"><i class="fa fa-save"></i></button>
                            </div>
                        </div>
                    </fieldshet>
                    <?= form_close(); ?>
                </div>
            </div>
        </div><!-- /.box-body -->
    </div><!-- /.box -->