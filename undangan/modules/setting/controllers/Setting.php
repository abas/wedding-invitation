<?php

defined('BASEPATH') OR EXIT('Bolo maapu juu. jao ijin');

class Setting extends MX_Controller {

    private $module;
    private $title;

    public function __construct() {
        parent::__construct();
        if (!$this->auth->sudah_login()){
            redirect('apps/login');
        }
        $this->module = 'setting';
        $this->load->model('m_invitation');
        $this->load->model('m_invitation_detail');
        $this->load->model('m_information');
    }

    public function index() {
        if ($this->input->post()){
            if ($this->input->post('prefix') == 'bride'){
                $this->save_bride(); 
            }
            if ($this->input->post('prefix') == 'groom'){
                $this->save_groom();
            }
            if ($this->input->post('prefix' == 'event')){
                $this->save_event();
            }
            // redirect('apps/settings','refresh');
        }
        $data['list'] = $this->m_invitation_detail->get_all();
        $data['event'] = $this->m_information->get('1');
        $data['bride'] = $this->m_invitation_detail->get_by(array('bride_groom'=>0));
        $data['groom'] = $this->m_invitation_detail->get_by(array('bride_groom'=>1));
        $data['title'] = $this->lang->line('settings');
        $data['modules'] = $this->module;
        $data['view_file'] = 'list';
        echo Modules::run('template', $data);
    }

    private function save_bride(){
        $config['upload_path']          = './uploads/images/';
        $config['allowed_types']        = 'gif|jpg|png|jpeg';
        $config['encrypt_name'] = TRUE;
        $bride_picture= $this->input->post('bride_old_pic');
        $this->load->library('upload', $config);

            if ($this->upload->do_upload('bride_picture')) {
                if (file_exists('./uploads/images/'.$bride_picture)){
                    unlink('./uploads/images/'.$bride_picture);
                }
                $bride_picture = $this->upload->file_name;
            }
        $bride_data = array(
            'full_name' => $this->input->post('bride_name'),
            'short_name' => $this->input->post('bride_short_name'),
            'address' => $this->input->post('bride_address'),
            'description' => $this->input->post('bride_description'),
            'picture' => $bride_picture
        );
        $this->m_invitation_detail->update($this->input->post('bride_id'),$bride_data);
    }

    private function save_groom(){
        $config['upload_path']          = './uploads/images/';
        $config['allowed_types']        = 'gif|jpg|png|jpeg';
        $config['encrypt_name'] = TRUE;
        $groom_picutre= $this->input->post('groom_old_pic');
        $this->load->library('upload', $config);

            if ($this->upload->do_upload('groom_picture')){
                if (file_exists('./uploads/images/'.$groom_picutre)){
                    unlink('./uploads/images/'.$groom_picutre);
                }
                $groom_picutre = $this->upload->file_name;
            }
        $groom_data = array(
            'full_name' => $this->input->post('groom_name'),
            'short_name' => $this->input->post('groom_short_name'),
            'address' => $this->input->post('groom_address'),
            'description' => $this->input->post('groom_description'),
            'picture' => $groom_picutre
        );
        $this->m_invitation_detail->update($this->input->post('groom_id'),$groom_data);
    }

    private function save_event(){
        $data = array(
            'name' => $this->input->post('name'),
            'location' => $this->input->post('location'),
            'date_information' => $this->input->post('date_information'),
            'time_information' => $this->input->post('time_information')
        );
        $this->m_information->update($this->input->post('id'),$data);
    }
}
