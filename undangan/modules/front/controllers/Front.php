<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Front extends MX_Controller {

    private $invitation_id = 1;

    public function __construct()
    {
        parent::__construct();
        $this->load->model('m_guests');
        $this->load->model('m_galery');
        $this->load->model('m_attendance');
        $this->load->model('m_invitation_detail');
    }

    public function index() {
        $token = $this->input->get('a');
        $guests = $this->m_guests->get_by(array('token'=>$token));
        $data['kepada'] = '';
        if (!empty($guests)){
            if ($guests->gender == 'MALE'){
                $alias = 'Mr. ';
            }else{
                if ($guests->marital_status == 'married'){
                    $alias = 'Mrs. ';
                }else{
                    $alias = 'Ms. ';
                }
            }
            $data['kepada'] = $alias.$guests->full_name;
            $data['guest_id'] = $guests->id;
            $this->m_guests->update($guests->id,array('read_status'=>TRUE));
        }else{
            redirect('apps/login','refresh');
        }
        $data['galery'] = $this->m_galery->get_all();
        $data['groom'] = $this->m_invitation_detail->get_by(array('bride_groom'=>1));
        $data['bride'] = $this->m_invitation_detail->get_by(array('bride_groom'=>0));
        $data['title'] = $data['groom']->short_name.' & '.$data['bride']->short_name.' Mengundang';
        $data['nav_title'] = $data['groom']->short_name.' & '.$data['bride']->short_name;
        echo Modules::run('template/frontend', $data);
    }

    public function save_attendance(){
        $token = $this->input->get('a');
        $attendance = array(
            'id' => uid(),
            'message' => $this->input->post('message'),
            'attend' => $this->input->post('attend'),
            'invitation_id' => $this->invitation_id,
            'guests_id' => $this->input->post('guest')
        );
        if (!$this->m_attendance->get_by(array('guests_id'=>$this->input->post('guest')))){
            $this->m_attendance->insert($attendance);
        }
        redirect(site_url().'?a='.$token,'refresh');
    }

}
