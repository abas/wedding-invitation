<!-- Default box -->
<div class="alert alert-info alert-dismissable">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <?= $this->lang->line('welcome').' '. $this->session->userdata('nama_lengkap'); ?>
</div>
<div class="col-md-3 col-sm-6 col-xs-12">
    <div class="info-box">
    <span class="info-box-icon bg-aqua"><i class="ion ion-ios-people-outline"></i></span>
    <div class="info-box-content">
        <span class="info-box-text"><?= $this->lang->line('guests_count') ?></span>
        <span class="info-box-number"><?= $guest_count; ?> <small><?= $this->lang->line('people'); ?></small></span>
    </div><!-- /.info-box-content -->
    </div><!-- /.info-box -->
</div>
<div class="col-md-3 col-sm-6 col-xs-12">
    <div class="info-box">
    <span class="info-box-icon bg-green"><i class="ion ion-ios-people-outline"></i></span>
    <div class="info-box-content">
        <span class="info-box-text"><?= $this->lang->line('will_attend') ?></span>
        <span class="info-box-number"><?= $will_attend; ?> <small><?= $this->lang->line('people'); ?></small></span>
    </div><!-- /.info-box-content -->
    </div><!-- /.info-box -->
</div>
<div class="col-md-3 col-sm-6 col-xs-12">
    <div class="info-box">
    <span class="info-box-icon bg-yellow"><i class="ion ion-ios-people-outline"></i></span>
    <div class="info-box-content">
        <span class="info-box-text"><?= $this->lang->line('maybe_attend') ?></span>
        <span class="info-box-number"><?= $maybe_attend; ?> <small><?= $this->lang->line('people'); ?></small></span>
    </div><!-- /.info-box-content -->
    </div><!-- /.info-box -->
</div>
<div class="col-md-3 col-sm-6 col-xs-12">
    <div class="info-box">
    <span class="info-box-icon bg-red"><i class="ion ion-ios-people-outline"></i></span>
    <div class="info-box-content">
        <span class="info-box-text"><?= $this->lang->line('not_attend') ?></span>
        <span class="info-box-number"><?= $not_attend; ?> <small><?= $this->lang->line('people'); ?></small></span>
    </div><!-- /.info-box-content -->
    </div><!-- /.info-box -->
</div>
<div class="col-md-3 col-sm-6 col-xs-12">
    <div class="info-box">
    <span class="info-box-icon bg-green"><i class="fa fa-envelope"></i></span>
    <div class="info-box-content">
        <span class="info-box-text"><?= $this->lang->line('invitation_read_count') ?></span>
        <span class="info-box-number"><a href="<?= site_url('apps/guests?rs=1') ?>"><?= $invitation_read_count; ?></a></span>
    </div><!-- /.info-box-content -->
    </div><!-- /.info-box -->
</div>