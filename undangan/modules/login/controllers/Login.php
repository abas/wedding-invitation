<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends MX_Controller {
    public function __construct()
    {
        parent::__construct();
        $this->load->library('auth');
    }

    public function index() {
        redirect('apps/login', 'refresh');
    }

    public function login_form() {
        if ($this->auth->sudah_login()) {
            redirect('apps/dashboard', 'refresh');
        }
        if ($this->input->post()) {
            $message = array(
                'required' => '%s tidak boleh kosong',
            );
            $this->form_validation->set_message($message);
            $this->form_validation->set_rules('identitas', 'Nama Pengguna', 'trim|required');
            $this->form_validation->set_rules('sandi', 'Kata Kunci', 'trim|required');
            $username = $this->input->post('identitas');
            $password = $this->input->post('sandi');
            if ($this->form_validation->run() === TRUE){
                if ($this->auth->login($username, $password)) {
                    redirect('apps/dashboard', 'refresh');
                }
            }else{
                $this->load->view('v_login');    
            }
        } else {
            $this->load->view('v_login');
        }
    }

    public function logout() {
        $this->auth->logout();
        $this->session->sess_destroy();
        redirect('apps/login', 'refersh');
    }

    public function home() {
        if (!$this->auth->sudah_login()) {
            redirect('apps/login', 'refresh');
        }
        $quests_count = query_sql("SELECT COUNT(id) AS total FROM guests","row");
        $guests_read_inv = query_sql("SELECT COUNT(id) AS total FROM guests WHERE read_status=1","row");
        $will_attend = query_sql("SELECT COUNT(id) AS total FROM attendance WHERE attend=0","row");
        $maybe_attend = query_sql("SELECT COUNT(id) AS total FROM attendance WHERE attend=1","row");
        $not_attend = query_sql("SELECT COUNT(id) AS total FROM attendance WHERE attend=2","row");
        $data['will_attend'] = $will_attend->total;
        $data['maybe_attend'] = $maybe_attend->total;
        $data['not_attend'] = $not_attend->total;
        $data['guest_count'] = $quests_count->total;
        $data['invitation_read_count'] = $guests_read_inv->total;
        $data['title'] = 'Dashboard';
        $data['page_header'] = 'Dashboard';
        $data['modules'] = 'login';
        $data['view_file'] = 'v_beranda';
        echo Modules::run('template', $data);
    }

    public function info() {
        echo 'Browser anda tidak mendukung javascript, silahkan baca literatur berikut <a href="http://www.enable-javascript.com/" target="_blank">ini</a> 
                    Untuk mengaktifkan javascript di browser anda';
    }

}
