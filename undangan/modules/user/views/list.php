<div class="row">
    <div class="col-md-8">
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Daftar Pengguna</h3>
            <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
            </div>
        </div>
        <div class="box-body">
            <?= $table; ?>
        </div><!-- /.box-body -->
    </div><!-- /.box -->
    </div>
    <div class="col-md-4">
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">Tambah Pengguna</h3>
                <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                </div>
            </div>
            <?= form_open('','class="form-horizontal" id="frmUser"') ?>
            <input type="hidden" name="id" id="id" value=""/>
            <input type="hidden" name="aksi" id="aksi" value="add"/>
            <div class="box-body">
                <div class="form-group">
                    <label for="username" class="control-label col-sm-4">Nama Pengguna</label>
                    <div class="col-sm-8">
                        <?= form_input('username',set_value('username'),'class="form-control" id="username" required') ?>
                    </div>
                </div>
                <div class="form-group">
                    <label for="password" class="control-label col-sm-4">Sandi</label>
                    <div class="col-sm-8">
                        <?= form_input('password',set_value('password'),'class="form-control" id="password" required','password') ?>
                    </div>
                </div>
                <div class="form-group">
                    <label for="email" class="control-label col-sm-4">Email</label>
                    <div class="col-sm-8">
                        <?= form_input('email',set_value('email'),'class="form-control" id="email" required','email') ?>
                    </div>
                </div>
                <div class="form-group">
                    <label for="nama_lengkap" class="control-label col-sm-4">Nama Lengkap</label>
                    <div class="col-sm-8">
                        <?= form_input('nama_lengkap',set_value('nama_lengkap'),'class="form-control" id="nama_lengkap" required') ?>
                    </div>
                </div>
                <div class="form-group">
                    <label for="status" class="control-label col-sm-4">Status</label>
                    <div class="col-sm-8">
                        <?= form_radio('status','1',TRUE) ?> Aktif &nbsp;
                        <?= form_radio('status','0',FALSE) ?> Tidak Aktif
                    </div>
                </div>
                <div class="form-group">
                    <label for="level" class="control-label col-sm-4">Level</label>
                    <div class="col-sm-8">
                        <?= form_dropdown('level',$list_level,set_value('level'),'class="form-control" id="level"') ?>
                    </div>
                </div>
            </div><!-- /.box-body -->
            <div class="box-footer">
                <button type="reset" class="btn btn-default">Batal</button>
                <button type="button" onclick="jvSave('User')" class="btn btn-info pull-right" id="btnAksi">Simpan</button>
            </div>
            <?= form_close(); ?>
        </div><!-- /.box -->
    </div>
</div>