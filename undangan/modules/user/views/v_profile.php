<div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title">Profil</h3>
    </div>
    <?= form_open('','class="form-horizontal" id="frmProfil"') ?>
    <div class="box-body">
    <?php if (!empty($this->session->flashdata('message'))) { ?>
        <div class="alert alert-info alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
             <?= $this->session->flashdata('message'); ?>
        </div>
    <?php }
    ?>
    <div class="row">
        <div class="col-md-6">
        <fieldset class="scheduler-border">
            <legend class="scheduler-border">Informasi Pnegguna</legend>
            <div class="form-group">
                <label for="username" class="control-label col-sm-4">Nama Pengguna</label>
                <div class="col-sm-5">
                    <?= form_input('username',set_value('username',$edit->username),'class="form-control" id="username" disabled') ?>
                </div>
            </div>
            <div class="form-group">
                <label for="password" class="control-label col-sm-4">Ganti Sandi</label>
                <div class="col-sm-5">
                    <?= form_input('password',set_value('password'),'class="form-control" id="password" placeholder="Kosongkan jika tidak ingin mengganti password"','password') ?>
                </div>
            </div>
            <div class="form-group">
                <label for="email" class="control-label col-sm-4">Email</label>
                <div class="col-sm-5">
                    <?= form_input('email',set_value('email',$edit->email),'class="form-control" id="email" required','email') ?>
                </div>
            </div>
            <div class="form-group">
                <label for="nama_lengkap" class="control-label col-sm-4">Nama Lengkap</label>
                <div class="col-sm-5">
                    <?= form_input('nama_lengkap',set_value('nama_lengkap',$edit->nice_name),'class="form-control" id="nama_lengkap" required') ?>
                </div>
            </div>
        </fieldset>
        </div>
        <div class="col-md-6">
            <fieldset class="scheduler-border">
                <legend class="scheduler-border">Data Bank</legend>
                <div class="form-group">
                    <label for="nama_bank" class="control-label col-sm-4">Nama Bank</label>
                    <div class="col-sm-5">
                        <?= form_input('nama_bank',set_value('nama_bank',$bank->nama_bank),'class="form-control" id="nama_bank" required') ?>
                    </div>
                </div>
                <div class="form-group">
                    <label for="no_rek" class="control-label col-sm-4">Nomor Rekening</label>
                    <div class="col-sm-5">
                        <?= form_input('no_rek',set_value('no_rek',$bank->no_rek),'class="form-control" id="no_rek" required') ?>
                    </div>
                </div>
                <div class="form-group">
                    <label for="nama_pemilik" class="control-label col-sm-4">Nama Pemilik Akun</label>
                    <div class="col-sm-5">
                        <?= form_input('nama_pemilik',set_value('nama_pemilik',$bank->nama_pemilik),'class="form-control" id="nama_pemilik" required') ?>
                    </div>
                </div>
            </fieldset>
        </div>
        <div class="col-md-12">
            <fieldset class="scheduler-border">
                <legend class="scheduler-border">Data Diri</legend>
                <div class="form-group">
                    <label for="nik" class="control-label col-sm-3">NIK</label>
                    <div class="col-sm-8">
                        <?= form_input('nik',set_value('nik',$profil->nik),'class="form-control" id="nik" required') ?>
                    </div>
                </div>
                <div class="form-group">
                    <label for="nama" class="control-label col-sm-3">Nama Lengkap</label>
                    <div class="col-sm-8">
                        <?= form_input('nama',set_value('nama',$profil->nama),'class="form-control" id="nama" required') ?>
                    </div>
                </div>
                <div class="form-group">
                    <label for="tempat_lahir" class="control-label col-sm-3">Tempat Lahir</label>
                    <div class="col-sm-8">
                        <?= form_input('tempat_lahir',set_value('tempat_lahir',$profil->tempat_lahir),'class="form-control" id="tempat_lahir" required') ?>
                    </div>
                </div>
                <div class="form-group">
                    <label for="tgl_lahir" class="control-label col-sm-3">Tanggal Lahir</label>
                    <div class="col-sm-8">
                        <?= form_input('tgl_lahir',set_value('tgl_lahir',$profil->tgl_lahir),'class="form-control" id="datemask" required') ?>
                    </div>
                </div>
                <div class="form-group">
                    <label for="jenis_kelamin" class="control-label col-sm-3">Jenis Kelamin</label>
                    <div class="col-sm-8">
                        <?= form_radio('jk','LAKI-LAKI',($profil->jenis_kelamin == 'LAKI-LAKI' ? TRUE : FALSE)) ?> LAKI-LAKI &nbsp;
                        <?= form_radio('jk','PEREMPUAN',($profil->jenis_kelamin == 'PEREMPUAN' ? TRUE : FALSE)) ?> PEREMPUAN
                    </div>
                </div>
                <div class="form-group">
                    <label for="alamat" class="control-label col-sm-3">Alamat</label>
                    <div class="col-sm-8">
                        <?= form_input('alamat',set_value('alamat',$profil->alamat),'class="form-control" id="alamat" required') ?>
                    </div>
                </div>
                <div class="form-group">
                    <label for="rt_rw" class="control-label col-sm-3">RT / RW</label>
                    <div class="col-sm-8">
                        <?= form_input('rt_rw',set_value('rt_rw',$profil->rt_rw),'class="form-control" id="rt_rw" required') ?>
                    </div>
                </div>
                <div class="form-group">
                    <label for="provinsi" class="control-label col-sm-3">Provinsi</label>
                    <div class="col-sm-2">
                        <?= form_dropdown('provinsi',$list_prov,set_value('provinsi',$profil->provinsi_id),'class="form-control" id="provinsi" onclick="get_wilayah(\'provinsi\',\'kabupaten\');" required') ?>
                    </div>
                    <div class="col-sm-2">
                        <?= form_dropdown('kabupaten',$list_kab,set_value('kabupaten',$profil->kabupaten_id),'class="form-control" id="kabupaten" onclick="get_wilayah(\'kabupaten\',\'kecamatan\');" ') ?>
                    </div>
                    <div class="col-sm-2">
                        <?= form_dropdown('kecamatan',$list_kec,set_value('kecamatan',$profil->kecamatan_id),'class="form-control" id="kecamatan" onclick="get_wilayah(\'kecamatan\',\'desa\');"') ?>
                    </div>
                    <div class="col-sm-2">
                        <?= form_dropdown('desa',$list_desa,set_value('desa',$profil->desa_id),'class="form-control" id="desa" ') ?>
                    </div>
                </div>
                <div class="form-group">
                    <label for="status_kawin" class="control-label col-sm-3">Jenis Kelamin</label>
                    <div class="col-sm-8">
                        <?= form_radio('status_kawin','Belum Kawin',($profil->status_kawin == 'Belum Kawin' ? TRUE : FALSE)) ?> Belum Nikah &nbsp;
                        <?= form_radio('status_kawin','Kawin',($profil->status_kawin == 'Kawin' ? TRUE : FALSE)) ?> Nikah &nbsp;
                        <?= form_radio('status_kawin','Janda/Duda',($profil->status_kawin == 'Janda/Duda' ? TRUE : FALSE)) ?> Janda/Duda &nbsp;
                    </div>
                </div>
                <div class="form-group">
                    <label for="pekerjaan" class="control-label col-sm-3">Pekerjaan</label>
                    <div class="col-sm-8">
                        <?= form_input('pekerjaan',set_value('pekerjaan',$profil->pekerjaan),'class="form-control" id="pekerjaan" required') ?>
                    </div>
                </div>
                <div class="form-group">
                    <label for="no_hp" class="control-label col-sm-3">No HP</label>
                    <div class="col-sm-8">
                        <?= form_input('no_hp',set_value('no_hp',$profil->no_hp),'class="form-control" id="no_hp" required') ?>
                    </div>
                </div>
            </fieldset>
        </div>
    </div>
    </div><!-- /.box-body -->
    <div class="box-footer">
        <a href="<?= site_url('apps/dashboard') ?>" data-toggle="tooltip" title="Kembali Ke Beranda" class="btn btn-default"><i class="fa fa-backward"></i></a>
        <button type="button" onclick="jvSave('Profil')" class="btn btn-info pull-right" id="btnAksi">Simpan</button>
    </div>
    <?= form_close(); ?>
</div><!-- /.box -->
