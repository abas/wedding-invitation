<?php

defined('BASEPATH') OR EXIT('Bolo maapu juu. jao ijin');

class User extends MX_Controller {

    private $module;
    private $view_file;
    private $level;
    private $title;
    private $menu;

    public function __construct() {
        parent::__construct();
        if (!$this->auth->sudah_login()) {
            redirect('apps/dashboard', 'refresh');
        }
        $this->module = 'user';
        $this->title = 'Data User';
        $this->level = $this->session->userdata('id_level');
    }

    public function index() {
        if ($this->input->post()) {
            $aksi = $this->input->post('aksi');
            $password = $this->input->post('password');
            $username = url_title($this->input->post('username'), '_', TRUE);
            $pesan = '';
            if ($aksi == 'edit') {
                if ($cek = $this->m_users->get($this->input->post('id'))) {
                    if (trim($password) && $password != "" && $cek->password != md5($password)) {
                        $password = md5($password);
                    } else {
                        $password = $cek->password;
                    }
                    $data = array(
                        'username' => $username,
                        'nice_name' => $this->input->post('nama_lengkap'),
                        'password' => $password,
                        'email' => $this->input->post('email'),
                        'level_id' => $this->input->post('level'),
                        'status' => $this->input->post('status')
                    );
                    if ($this->m_users->update($cek->id, $data)) {
                        $pesan = 'Berhasil Rubah Data';
                    } else {
                        $pesan = 'Gagal Rubah Data';
                    }
                }
            } else if ($aksi == 'add') {
                if ($this->m_users->get_by(array('username' => $username))) {
                    $pesan = 'Gagal Tambah Data Karena Username Sudah Digunakan';
                } else {
                    $id_user = auto_inc('m_users', 'id');
                    $id_profil = auto_inc('m_profil','id');
                    $id_agen = auto_inc('m_agen','id');
                    $level = $this->m_levels->get($this->input->post('level'));
                    $jabatan = $this->m_jabatan->get_by(array('singkatan'=>$level->nama));
                    $data = array(
                        'id' => $id_user,
                        'username' => $username,
                        'nice_name' => $this->input->post('nama_lengkap'),
                        'password' => md5($password),
                        'email' => $this->input->post('email'),
                        'status' => 1,
                        'level_id' => $this->input->post('level') 
                    );
                    if ($this->m_users->insert($data)) {
                        $data_agen = array(
                            'id' => $id_agen,
                            'kode' => uid(),
                            'parent' => 0,
                            'jabatan_id' => $jabatan->id,
                            'users_id' => $id_user,
                            'status_agen_id' => 1,
                            'profil_id' => $id_profil
                        );
                        $this->m_profil->insert(array('id'=>$id_profil,'nama'=>$this->input->post('nama_lengkap')));
                        $this->m_agen->insert($data_agen);
                        $pesan = 'Berhasil Tambah Data';
                    } else {
                        $pesan = 'Gagal Tambah Data';
                    }
                }
            } else {
                
            }
            $this->session->set_flashdata(array('pesan' => $pesan));
            redirect('apps/master/pengguna', 'refresh');
        }
        $header = array('No', 'Nama Pengguna', 'Nama Lengkap', 'Email', 'Status', 'Level', '#');
        $tmpl = array(
            'table_open' => '<table ' . style('table') . ' cellpadding="2" cellspacing="2">',
            'thead_open' => '<thead class="table-inverse"');
        $this->table->set_template($tmpl);
        $this->table->set_heading($header);
        $list_user = query_sql("SELECT a.*,b.nama AS level FROM users a LEFT JOIN level b ON (a.level_id=b.id)");
        if (!empty($list_user)) {
            $no = 1;
            foreach ($list_user as $val) {
                $josn_data = array(
                    'id' => $val->id,
                    'username' => $val->username,
                    'fullname' => $val->nice_name,
                    'email' => $val->email,
                    'status' => $val->status,
                    'level' => $val->level_id
                );
                $arr_confirm = array(
                    'pesan' => 'Apakah anda yakin ingin menghapus data ini ? ',
                    'url' => site_url('apps/master/pengguna/hapus/' . $val->id),
                    'judul' => 'Peringatan'
                );
                $button_data = array(
                    'url_edit' => '#',
                    'url_hapus' => '#',
                    'json_data' => $josn_data,
                    'extra_class' => 'edituser',
                    'konfirmasi_hapus' => $arr_confirm
                );
                $this->table->add_row($no++, $val->username, $val->nice_name, $val->email, ($val->status == 1 ? '<center><a href="' . site_url() . "apps/master/pengguna/status/" . $val->id . '/0" ' . style('popover', 'Non Aktifkan') . '><i class="fa fa-unlock"></i></a></center>' : '<center><a href="' . site_url() . "apps/master/pengguna/status/" . $val->id . '/1" ' . style('popover', 'Aktifkan') . '><i class="fa fa-lock"></i></a></center>'), $val->level, get_btn($button_data));
            }
        }
        $data['table'] = $this->table->generate();
        $data['list_level'] = drop_list('m_levels', 'id', 'nama', 'Pilih Level');
        $data['footer_width'] = 'container-fluid';
        $data['title'] = 'Pengguna';
        $data['modules'] = $this->module;
        $data['menu'] = $this->menu;
        $data['view_file'] = 'list';
        echo Modules::run('template', $data);
    }

    public function update_status($id, $status) {
        if ($this->m_users->get($id)) {
            $this->m_users->update($id, array('status' => $status));
        }
        redirect('apps/master/pengguna', 'refresh');
    }

    public function profile() {
        $id = $this->session->userdata('id');
        $cek = $this->m_users->get($id);
        $agen = $this->m_agen->get_by(array('users_id'=>$cek->id));
        $bank = $this->m_bank->get_by(array('agen_id'=>$agen->id));
        $profil = $this->m_profil->get($agen->profil_id);
        if ($this->input->post()) {
            if (!empty($this->input->post('password'))){
                $password = md5($this->input->post('password'));
            }else{
                $password = $cek->password;
            }
            $data_edit = array(
                'email' => $this->input->post('email'),
                'nice_name' => $this->input->post('nama_lengkap'),
                'updated_date' => strtotime(date('Y-m-d H:i:s')),
                'password' => $password
            );
            $data_bank = set_data_bank($bank->id,$agen->id);
            $data_profil = set_data_profil($profil->id);
            if ($this->m_users->update($id, $data_edit)) {
                $this->m_bank->update($bank->id,$data_bank);
                $this->m_profil->update($profil->id,$data_profil);
                $this->session->set_userdata(array('nama_lengkap' => $this->input->post('nama_lengkap')));
                $this->session->set_flashdata(array('message' => 'Perubahan Berhasil'));
            } else {
                $this->session->set_flashdata(array('message' => 'Perubahan Gagal'));
            }
        }
        $data['list_prov'] = drop_list('m_provinsi','id','nama','Pilih Provinsi');
        $data['list_kab'] = drop_list('m_kabupaten','id','nama','Pilih Kabupaten','get_many_by',array('provinsi_id'=>$profil->provinsi_id));
        $data['list_kec'] = drop_list('m_kecamatan','id','nama','Pilih Kecamatan','get_many_by',array('kabupaten_id'=>$profil->kabupaten_id));
        $data['list_desa'] = drop_list('m_desa','id','nama','Pilih Desa / Kelurahan','get_many_by',array('kecamatan_id'=>$profil->kecamatan_id));
        $data['edit'] = $cek;
        $data['bank'] = $bank;
        $data['profil'] = $profil;
        $data['title'] = 'Profile';
        $data['modules'] = $this->module;
        $data['view_file'] = 'v_profile';
        echo Modules::run('template', $data);
    }

    public function delete($id) {
        if ($this->m_users->get($id)) {
            $this->m_users->delete($id);
            redirect('apps/master/pengguna');
        } else {
            redirect('apps/dashboard', 'refresh');
        }
    }

}
