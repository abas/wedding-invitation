<?php
$route['apps/master/pengguna'] = 'user';
$route['apps/master/pengguna/load'] = 'user/load_user';
$route['apps/master/pengguna/hapus/(:any)'] = 'user/delete/$1';
$route['apps/master/pengguna/status/(:any)/(:any)'] = 'user/update_status/$1/$2';
$route['apps/profile'] = 'user/profile';
$route['user'] = 404;
$route['user/load_user'] = 404;
$route['user/delete/$1'] = 404;
$route['user/update_status/$1/$2'] = 404;
