<?php 
$path = THEMEPATH.DIRECTORY_SEPARATOR.get_active_theme();
$theme_path = $path;
if(is_dir($path)):
    if(is_file($path.'/template.php')):
        include THEMEPATH.'/'.get_active_theme().'/template.php';
        else:
            echo 'No folder';
        endif;
    else:
        include THEMEPATH.DIRECTORY_SEPARATOR.'/default/index.html';
    endif;
?>
