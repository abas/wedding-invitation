<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title><?= $this->lang->line('title') ?> | <?= $title; ?></title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <link href="<?= base_url(); ?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url(); ?>assets/css/AdminLTE.min.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url(); ?>assets/css/select2.min.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url(); ?>assets/css/skins/_all-skins.min.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url(); ?>assets/css/custom.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url(); ?>assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css" rel="stylesheet" type="text/css" />
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
    .tiptop {
      position: relative;
      display: inline-block;
    }

    .tiptop .tiptoptext {
      visibility: hidden;
      width: 140px;
      background-color: #555;
      color: #fff;
      text-align: center;
      border-radius: 6px;
      padding: 5px;
      position: absolute;
      z-index: 1;
      bottom: 150%;
      left: 50%;
      margin-left: -75px;
      opacity: 0;
      transition: opacity 0.3s;
    }

    .tiptop .tiptoptext::after {
      content: "";
      position: absolute;
      top: 100%;
      left: 50%;
      margin-left: -5px;
      border-width: 5px;
      border-style: solid;
      border-color: #555 transparent transparent transparent;
    }

    .tiptop:hover .tiptoptext {
      visibility: visible;
      opacity: 1;
    }
  </style>
  </head>
  <body class="skin-green sidebar-mini">
    <div class="wrapper">
      
      <?= $this->load->view('header') ?>
      <?= $this->load->view('sidebar') ?>
      <div class="content-wrapper">
        <section class="content">
          <?= $this->load->view($modules.'/'.$view_file); ?>
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

      <footer class="main-footer">
        <div class="pull-right hidden-xs">
          <b>Version</b> beta
        </div>
        <strong>Copyright &copy; <?= date('Y') ?> <a href="http://gorontaloit.info">Gorontalo<strong>IT</strong></a>.</strong> All rights reserved.
      </footer>

      <?= bs_confirm_content(); ?>
      <?= bs_info_content(); ?>
    </div><!-- ./wrapper -->

    <!-- jQuery 2.1.4 -->
    <script src="<?= base_url(); ?>assets/plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.2 JS -->
    <script src="<?= base_url(); ?>assets/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <!-- SlimScroll -->
    <script src="<?= base_url(); ?>assets/plugins/slimScroll/jquery.slimscroll.min.js" type="text/javascript"></script>
    <!-- FastClick -->
    <script src='<?= base_url(); ?>assets/plugins/fastclick/fastclick.min.js'></script>
    <script src='<?= base_url(); ?>assets/plugins/datatables/datatables.min.js'></script>
    <script src='<?= base_url(); ?>assets/plugins/inputmask/jquery.inputmask.bundle.js'></script>
    <script src='<?= base_url(); ?>assets/js/select2.min.js'></script>
    <script src='<?= base_url(); ?>assets/js/jquery.validate.js'></script>
    <!-- AdminLTE App -->
    <script src="<?= base_url(); ?>assets/js/app.min.js" type="text/javascript"></script>
    <script src="<?= base_url(); ?>assets/js/function.js" type="text/javascript"></script>
    <script src="<?= base_url(); ?>assets/js/wilayah.js" type="text/javascript"></script>
    <script src="<?= base_url(); ?>assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js" type="text/javascript"></script>
    <script type="text/javascript">
      $(function () {
        $(".wys5").wysihtml5();
      });
      // var copyBtn   = $("#copy-btn"),
      //     input     = $("#copy-me");

      function copyToClipboardFF(text) {
        window.prompt ("Copy to clipboard: Ctrl C, Enter", text);
      }

      function copyToClipboard(id) {
        var success   = true,
            range     = document.createRange(),
            input     = $("#copy-me"+id),
            selection;

        // For IE.
        if (window.clipboardData) {
          window.clipboardData.setData("Text", input.val());        
        } else {
          // Create a temporary element off screen.
          var tmpElem = $('<div>');
          tmpElem.css({
            position: "absolute",
            left:     "-1000px",
            top:      "-1000px",
          });
          // Add the input value to the temp element.
          tmpElem.text(input.val());
          $("body").append(tmpElem);
          // Select temp element.
          range.selectNodeContents(tmpElem.get(0));
          selection = window.getSelection ();
          selection.removeAllRanges ();
          selection.addRange (range);
          // Lets copy.
          try { 
            success = document.execCommand ("copy", false, null);
          }
          catch (e) {
            copyToClipboardFF(input.val());
          }
          if (success) {
            var tooltip = document.getElementById("myTiptop"+id);
            tooltip.innerHTML = "Copied";
            // remove temp element.
            tmpElem.remove();
          }
        }
      }

// copyBtn.on('click', copyToClipboard);
    </script>
    <?= bs_confirm_js() ?>
    <?= bs_info_js() ?>
  </body>
</html>