<aside class="main-sidebar">
        <section class="sidebar">
          
          <ul class="sidebar-menu">
            <li class="header">MENU UTAMA</li>
            <li>
              <a href="<?= site_url('apps/dashboard'); ?>">
                <i class="fa fa-home"></i> <span>Home</span>
              </a>
            </li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-tags"></i>
                <span><?= $this->lang->line('guests') ?> </span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="<?= site_url('apps/guests') ?>"><i class="fa fa-circle-o"></i> List</a></li>
                <li><a href="<?= site_url('apps/guests/new') ?>"><i class="fa fa-circle-o"></i> <?= $this->lang->line('new_guests') ?> </a></li>
              </ul>
            </li>       
            <li>
              <a href="<?= site_url('apps/attendance'); ?>">
                <i class="fa fa-book"></i> <span><?= $this->lang->line('attendance') ?> </span>
              </a>
            </li>            
            <li>
              <a href="<?= site_url('apps/galery'); ?>">
                <i class="fa fa-camera"></i> <span><?= $this->lang->line('galery') ?> </span>
              </a>
            </li>
            <li>
              <a href="<?= site_url('apps/settings'); ?>">
                <i class="fa fa-wrench"></i> <span><?= $this->lang->line('settings') ?> </span>
              </a>
            </li>
          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>