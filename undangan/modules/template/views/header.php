<header class="main-header">
        <a href="<?= site_url(); ?>" class="logo">
          <span class="logo-mini"><strong>I</strong></span>
          <span class="logo-lg"><b><?= $this->lang->line('title') ?></b></span>
        </a>
        <nav class="navbar navbar-static-top" role="navigation">
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </a>
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <span class="hidden-xs"><?= $this->session->userdata('nama_lengkap') ?></span>
                </a>
                <ul class="dropdown-menu">
                <li><a href="<?= site_url('apps/logout') ?>">Keluar</a></li>
                </ul>
              </li>
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <span class="fa fa-globe"> <?= $this->lang->line('language') ?></span>
                </a>
                <ul class="dropdown-menu">
                  <li><a href="<?= site_url('apps/language/ID') ?>">Indonesia</a></li>
                  <li><a href="<?= site_url('apps/language/EN') ?>">English</a></li>
                </ul>
              </li>
            </ul>
          </div>
        </nav>
      </header>