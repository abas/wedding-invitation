<?php

defined('BASEPATH') OR EXIT('Bolo maapu juu. jao ijin');

class Template extends MX_Controller {
    
    public function __construct() {
        parent::__construct();
    }

    public function index($data) {
        $this->load->view('v_template', $data);
    }

    public function frontend($data) {
        $this->load->view('v_frontend', $data);
    }
}
