<?php

defined('BASEPATH') OR EXIT('Bolo maapu juu. jao ijin');

class Attendance extends MX_Controller {

    private $module;
    private $title;

    public function __construct() {
        parent::__construct();
        if (!$this->auth->sudah_login()){
            redirect('apps/login');
        }
        $this->module = 'attendance';
        $this->title = 'List';
        $this->load->model('m_attendance');
    }

    public function index() {
        $header = array('No', $this->lang->line('full_name'), $this->lang->line('attend'),$this->lang->line('message'),'#');
        set_table($header);
        $list = query_sql("SELECT * FROM v_attendance");
        if (!empty($list)) {
            $no = 1;
            foreach ($list as $val) {
                if($val->attend == 0){
                    $attend = 'will_attend';
                }else if($val->attend == 1){
                    $attend = 'maybe_attend';
                }else{
                    $attend = 'not_attend';
                }
                $arr_confirm = array(
                    'pesan' => $this->lang->line('warning_content'),
                    'url' => site_url('apps/attendance/' . $val->id.'/delete'),
                    'judul' => $this->lang->line('warning_title')
                );
                $btn = '<a href="' .site_url('apps/attendance/'.$val->id.'/detail') . '" data-toggle="tooltip" title="'.$this->lang->line('detail').'"><i class="fa fa-eye"></i></a> &nbsp;';
                $btn .= '<a href="#" class="" ' . bs_confirm($arr_confirm) . ' data-toggle="tooltip" title="'.$this->lang->line('delete').'"><i class="fa fa-trash"></i></a> &nbsp;';
                $this->table->add_row($no++, $val->full_name,$this->lang->line($attend),substr($val->message,0,100).'...',$btn);
            }
        }
        $this->title = $this->title.' '.$this->lang->line('attendance');
        $data['table'] = $this->table->generate();
        $data['title'] = $this->title;
        $data['modules'] = $this->module;
        $data['view_file'] = 'list';
        echo Modules::run('template', $data);
    }

    public function detail($id){
        if($cek = query_sql("SELECT * FROM v_attendance WHERE id=$id","row")){
            $this->title = 'Detail '.$this->lang->line('attendance');
            $data['detail'] = $cek;
            $data['table'] = $this->table->generate();
            $data['title'] = $this->title;
            $data['modules'] = $this->module;
            $data['view_file'] = 'v_detail';
            echo Modules::run('template', $data);
        }
    }

    public function delete($id) {
        if ($cek = $this->m_attendance->get($id)) {
                $this->m_attendance->delete($id);
        }
        redirect('apps/attendance', 'refresh');
    }

}
