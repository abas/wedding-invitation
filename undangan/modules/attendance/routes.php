<?php
$route['apps/attendance'] = 'attendance';
$route['apps/attendance/(:any)/delete'] = 'attendance/delete/$1';
$route['apps/attendance/(:any)/detail'] = 'attendance/detail/$1';
$route['attendance'] = 404;
$route['attendance/detail/$1'] = 404;
$route['attendance/delete/$1'] = 404;