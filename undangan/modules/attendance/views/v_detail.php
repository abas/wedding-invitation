<?php 
if($detail->attend == 0){
    $attend = 'will_attend';
}else if($detail->attend == 1){
    $attend = 'maybe_attend';
}else{
    $attend = 'not_attend';
}
?>
<div class="box">
        <div class="box-header with-border">
            <h3 class="box-title"><?= $title ?></h3>
        </div>
            <div class="box-body">
                <dl class="dl-horizontal">         
                    <dt><?= $this->lang->line('full_name') ?></dt>
                    <dd><?= $detail->full_name ?></dd>
                    <dt><?= $this->lang->line('attend') ?></dt>
                    <dd><?= $this->lang->line($attend) ?></dd>
                    <dt><?= $this->lang->line('message') ?></dt>
                    <dd><?= $detail->message ?></dd>
                </dl>
            </div><!-- /.box-body -->
            <div class="box-footer">
                <a href="#" onclick="location.replace(document.referrer)" class="btn btn-default" data-toggle="tooltip" title="<?= $this->lang->line('back') ?>"><i class="fa fa-backward"></i></a>
            </div>
    </div><!-- /.box -->