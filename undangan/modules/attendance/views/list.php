<div class="box">
        <div class="box-header with-border">
            <h3 class="box-title"><?= $title ?></h3>
        </div>
        <div class="box-body">
            <?= $table; ?>
        </div><!-- /.box-body -->
    </div><!-- /.box -->