<?php
$route['apps/galery'] = 'galery';
$route['apps/galery/new'] = 'galery/add';
$route['apps/galery/(:any)/delete'] = 'galery/delete/$1';
$route['apps/galery/(:any)/update'] = 'galery/update/$1';
$route['galery'] = 404;
$route['galery/add'] = 404;
$route['galery/update/$1'] = 404;
$route['galery/detail/$1'] = 404;
$route['galery/delete/$1'] = 404;