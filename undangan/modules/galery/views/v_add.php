<div class="box">
        <div class="box-header with-border">
            <h3 class="box-title"><?= $title ?></h3>
        </div>
        <?= form_open_multipart('','class="form-horizontal" id="formPerson"') ?>
            <div class="box-body">
            <div class="form-group">
                <label for="nama" class="control-label col-sm-3"><?= $this->lang->line('label_title') ?></label>
                <div class="col-sm-5">
                    <?= form_input('title',set_value('title'),'class="form-control" id="nama" required') ?>
                </div>
            </div>
            <div class="form-group">
                <label for="nama" class="control-label col-sm-3"><?= $this->lang->line('image') ?></label>
                <div class="col-sm-5">
                <input type="file" name="galery" size="20" />
                    <small><?= $error;?></small>
                </div>
            </div>
            </div><!-- /.box-body -->
            <div class="box-footer">
                <a href="<?= site_url('apps/galery') ?>" class="btn btn-default" data-toggle="tooltip" title="<?= $this->lang->line('back') ?>"><i class="fa fa-backward"></i></a>
                <button type="submit" class="btn btn-info pull-right" data-toggle="tooltip" title="<?= $this->lang->line('save') ?>"><i class="fa fa-save"></i></button>
            </div>
            <?= form_close(); ?>
    </div><!-- /.box -->