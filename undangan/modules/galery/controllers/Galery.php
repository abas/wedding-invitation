<?php

defined('BASEPATH') OR EXIT('Bolo maapu juu. jao ijin');

class Galery extends MX_Controller {

    private $module;
    private $title;

    public function __construct() {
        parent::__construct();
        if (!$this->auth->sudah_login()){
            redirect('apps/login');
        }
        $this->module = 'galery';
        $this->title = 'List';
        $this->load->model('m_galery');
    }

    public function index() {
        $header = array('No', $this->lang->line('label_title'), $this->lang->line('image'),'#');
        set_table($header);
        $list = $this->m_galery->get_all();
        if (!empty($list)) {
            $no = 1;
            foreach ($list as $val) {
                $arr_confirm = array(
                    'pesan' => $this->lang->line('warning_content'),
                    'url' => site_url('apps/galery/' . $val->id.'/delete'),
                    'judul' => $this->lang->line('warning_title')
                );
                $btn = '<a href="' .site_url('apps/galery/'.$val->id.'/update') . '" data-toggle="tooltip" title="'.$this->lang->line('update').'"><i class="fa fa-edit"></i></a> &nbsp;';
                $btn .= '<a href="#" class="" ' . bs_confirm($arr_confirm) . ' data-toggle="tooltip" title="'.$this->lang->line('delete').'"><i class="fa fa-trash"></i></a> &nbsp;';
                $this->table->add_row($no++, $val->title, '<img src="'.base_url('uploads/images/'.$val->encrypt_name).'" width="100">',$btn);
            }
        }
        $this->title = $this->title.' '.$this->lang->line('galery');
        $data['table'] = $this->table->generate();
        $data['title'] = $this->title;
        $data['modules'] = $this->module;
        $data['view_file'] = 'list';
        echo Modules::run('template', $data);
    }

    public function add(){
        $this->title= $this->lang->line('new').' '.$this->lang->line('galery');
        $config['upload_path']          = './uploads/images/';
        $config['allowed_types']        = 'gif|jpg|png|jpeg';
        $config['encrypt_name'] = TRUE;

        $this->load->library('upload', $config);

        if ( ! $this->upload->do_upload('galery'))
        {
                $data['error'] = $this->upload->display_errors();
                $data['title'] = $this->title;
                $data['modules'] = $this->module;
                $data['view_file'] = 'v_add';
                echo Modules::run('template', $data);
        }
        else
        {
            $new_galery = array(
                'id' => auto_inc('m_galery','id'),
                'title' => $this->input->post('title'),
                'orig_name' => $this->upload->orig_name,
                'encrypt_name' =>$this->upload->file_name,
                'invitation_id' => 1
            );
            $this->m_galery->insert($new_galery);
            redirect('apps/galery/new','refresh');
        }
    }

    public function update($id){
        if ($cek = $this->m_galery->get($id)) {
            $this->title= $this->lang->line('update').' '.$this->lang->line('galery');
            $config['upload_path']          = './uploads/images/';
            $config['allowed_types']        = 'gif|jpg|png|jpeg';
            $config['encrypt_name'] = TRUE;

            $this->load->library('upload', $config);

            if ( ! $this->upload->do_upload('galery'))
            {
                if ($this->input->post()){
                    $this->m_galery->update($id,array('title'=>$this->input->post('title')));
                    redirect('apps/galery','refresh');
                }
                $data['edit'] = $cek;
                $data['title'] = $this->title;
                $data['modules'] = $this->module;
                $data['view_file'] = 'v_update';
                echo Modules::run('template', $data);
            }
            else
            {
                unlink('./uploads/images/'.$cek->encrypt_name);
                $new_galery = array(
                    'title' => $this->input->post('title'),
                    'orig_name' => $this->upload->orig_name,
                    'encrypt_name' =>$this->upload->file_name
                );
                $this->m_galery->update($id,$new_galery);
                redirect('apps/galery','refresh');
            }
        }else{
            redirect('apps/galery','refresh');
        }
    }

    public function delete($id) {
        if ($cek = $this->m_galery->get($id)) {
            $upload_path ='./uploads/images/';
            if (is_file($upload_path.$cek->encrypt_name)){
                unlink($upload_path.$cek->encrypt_name);
                $this->m_galery->delete($id);
            }
        }
        redirect('apps/galery', 'refresh');
    }

}
