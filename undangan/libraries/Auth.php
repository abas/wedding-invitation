<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
define('STATUS_ACTIVATED', '1');
define('STATUS_NOT_ACTIVATED', '0');
define('ALLOW', TRUE);
define('NOT_ALLOW', FALSE);

Class Auth {

    private $ci;
    private $error = array();

    public function __construct() {
        $this->ci = & get_instance();
    }

    public function login($username, $password) {
        if ((strlen($username) > 0) AND ( strlen($password) > 0)) {
            if ($userr = query_sql("SELECT * FROM users where username = '" . $username . "'", "row")) {
                $user = $userr->username;
                $nl = $userr->nice_name;
                $status = $userr->status;
                $email = $userr->email;
                $pass = $userr->password;
                $kd_user = $userr->id;
                $level = $userr->level_id;
                if (md5($password) == $pass) {
                    $this->ci->session->set_userdata(array(
                        'username' => $user,
                        'user_id' => $kd_user,
                        'nama_lengkap' => $nl,
                        'level' => $level,
                        'email' => $email,
                        'tahun' => date('Y'),
                        'tahun_depan' => date('Y') + 1,
                        'status' => ($status == 1) ? STATUS_ACTIVATED : STATUS_NOT_ACTIVATED
                    ));

                    if ($status == 0 || $level == 0) {
                        echo "<script>alert('User belum aktif. Silahkan Hubungi Admin!!!');
			window.location=('" . site_url('apps/login') . "');</script>";
                    } else {
                        return true;
                    }
                }else{
                    echo "<script>alert('Password Salah');
                    window.location=('" . site_url('apps/login') . "');</script>";
                }
            } else {
                echo "<script>alert('Username salah!');
			window.location=('" . site_url('apps/login') . "');</script>";
            }
        }
        return FALSE;
    }

    public function logout() {
        $this->ci->session->set_userdata(array('username' => '', 'status' => '', 'email' => '', 'nama_lengkap' => '', 'id' => '', 'level' => '', 'tahun'=>'','tahun_depan'=>''));
        $this->ci->session->sess_destroy();
    }

    public function sudah_login($activated = TRUE) {
        return $this->ci->session->userdata('status') === ($activated ? STATUS_ACTIVATED : STATUS_NOT_ACTIVATED);
    }

    public function role($level = array()) {
        $status = in_array($this->ci->session->userdata('level'), $level) ? ALLOW : NOT_ALLOW;
        return $status;
    }

}
