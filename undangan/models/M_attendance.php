<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_attendance extends MY_Model {

    public function __construct() {
        parent::__construct();
        parent::set_table('attendance', 'id',TRUE);
    }

    public function new(){
        $data_new = array(
            'id' => uid(),
            'message' => $this->input->post('ucapan'),
            'attend' => $this->input->post('attendace'),
            'invitation_id' => $this->input->post('invitation'),
            'guests_id' => $this->input->post('guests')
        );
        if(parent::insert($data_new)){
            return true;
        }
        return false;
    }
    
}