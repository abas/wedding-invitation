<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_guests extends MY_Model {

    public function __construct() {
        parent::__construct();
        parent::set_table('guests', 'id',TRUE);
    }

    public function new(){
        $new_data = array(
            'id' => $this->input->post("id"),
            'full_name' => $this->input->post('nama_lengkap'),
            'gender' => $this->input->post('jenis_kelamin'),
            'degree' => $this->input->post('gelar'),
            'phone_number' => $this->input->post('no_hp'),
            'address' => $this->input->post('alamat'),
            'token' => $this->uuid->v5($this->input->post('nama_lengkap')),
            'marital_status' => $this->input->post('marital_status')
        );
        if (parent::insert($new_data)){
            return true;
        }else{
            return false;
        }
    }

    public function change(){
        $data_change = array(
            'full_name' => $this->input->post('nama_lengkap'),
            'gender' => $this->input->post('jenis_kelamin'),
            'degree' => $this->input->post('gelar'),
            'phone_number' => $this->input->post('no_hp'),
            'address' => $this->input->post('alamat'),
            'marital_status' => $this->input->post('marital_status')
        );
        if (parent::update($this->input->post('id'),$data_change)){
            return true;
        }else{
            return false;
        }
    }
}