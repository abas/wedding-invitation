<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_invitation_detail extends MY_Model {

    public function __construct() {
        parent::__construct();
        parent::set_table('invitation_detail', 'id',TRUE);
    }
}