<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_sosmed extends MY_Model {

    public function __construct() {
        parent::__construct();
        parent::set_table('sosmed', 'id',TRUE);
    }

    public function new(){
        $data_new = array(
            'id' => $this->input->post('id_sosmed'),
            'facebook' => $this->input->post('facebook'),
            'instagram' => $this->input->post('instagram'),
            'twitter' => $this->input->post('twitter'),
            'guests_id' => $this->input->post('id')
        );
        if(parent::insert($data_new)){
            return true;
        }else{
            return false;
        }
    }

    public function change(){
        $data_change = array(
            'facebook' => $this->input->post('facebook'),
            'instagram' => $this->input->post('instagram'),
            'twitter' => $this->input->post('twitter')
        );
        if(parent::update($this->input->post('id_sosmed'),$data_change)){
            return true;
        }else{
            return false;
        }
    }
    
}