<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_information extends MY_Model {

    public function __construct() {
        parent::__construct();
        parent::set_table('information', 'id',TRUE);
    }

    public function new(){
        $new_data = array(
            'id' => auto_inc('m_information','id'),
            'name' => $this->input->post('name'),
            'location' => $this->input->post('location'),
            'date_information' => $this->input->post('date_information'),
            'time_information' => $this->input->post('time_information'),
            'invitation_id' => $this->input->post('invitation')
        );
        if(parent::insert($new_data)){
            return true;
        }
        return false;
        
    }
}