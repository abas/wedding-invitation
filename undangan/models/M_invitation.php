<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_invitation extends MY_Model {

    public function __construct() {
        parent::__construct();
        parent::set_table('invitation', 'id',TRUE);
    }

    public function new(){
        $user_id = $this->session->userdata('user_id');

        if(!parent::get_by(array('users_id'=>$user_id))){
            $new_data = array(
                'users_id' => $user_id,
                'siteurl' => site_url()
            );
            if(parent::insert($new_data)){
                return true;
            }
            return false;
        }
        return true;
        
    }
}