<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = 'front';
$routes = 'routes.php';
$dir = APPPATH . DIRECTORY_SEPARATOR . 'modules/';
$list_modules = scandir($dir);
if (is_array($list_modules)) {
    foreach ($list_modules as $val) {
        if (!is_file($val) && file_exists($dir . $val . DIRECTORY_SEPARATOR . $routes)) {
            include $dir . $val . DIRECTORY_SEPARATOR . $routes;
        }
    }
}
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
