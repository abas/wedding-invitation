<?php
class LanguageLoader{
    function initialize(){
        $ci =& get_instance();
        $ci->load->helper('language');
        $siteLang = $ci->session->userdata('site_lang');
        if($siteLang && is_dir(APPPATH.'/language/'.$siteLang)){
            $ci->lang->load('site',$siteLang);
            $ci->lang->load('label',$siteLang);
        }else{
            $ci->lang->load('site','english');
            $ci->lang->load('label','english');
        }
    }
}