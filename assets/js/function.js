$(document).ready(function () {
    baseURL = "http://" + location.host + "/hizbulwasail.id/";
    $('#maskdate').inputmask('99-99-9999', {placeholder: "dd-mm-yyyy"});
    $('.select2').select2();
    $('#harga').inputmask("numeric", {
        radixPoint: ".",
        groupSeparator: ",",
        digits: 2,
        autoGroup: true,
        rightAlign: false,
        oncleared: function () {
            self.Value('');
        }
    });
    $(".edituser").click(function () {
        let data = JSON.parse($(this).attr('data'));
        $("#aksi").val("edit");
        $("#id").val(data.id);
        $("#nama_lengkap").val(data.fullname);
        $("#email").val(data.email);
        $("#username").val(data.username);
        $("#level").val(data.level).trigger('change');
        $("#password").prop("placeholder", "Ganti Password");
        $("#password").removeAttr('required');
        $("#btnAksi").html('Simpan Perubahan');
    });
    $('#myTable').DataTable({
        "responsive": true,
        "pageLength": 50,
        "language": {
            "lengthMenu": "Tampilkan _MENU_ data per halaman",
            "zeroRecords": "Tidak ada data",
            "info": "Menampilkan (_START_ - _END_) dari _TOTAL_ data",
            "infoEmpty": "",
            "search": "Cari Data :",
            "infoFiltered": "(filtered from _MAX_ total records)",
            "paginate": {
                "previous": "Sebelumnya",
                "next": "Selanjutnya"
            }
        }
    });
    $('#myTable2').DataTable({
        "responsive": true,
        "pageLength": 50,
        "language": {
            "lengthMenu": "Tampilkan _MENU_ data per halaman",
            "zeroRecords": "Tidak ada data",
            "info": "Menampilkan (_START_ - _END_) dari _TOTAL_ data",
            "infoEmpty": "",
            "search": "Cari Data :",
            "infoFiltered": "(filtered from _MAX_ total records)",
            "paginate": {
                "previous": "Sebelumnya",
                "next": "Selanjutnya"
            }
        }
    });
});
function jvSave(frm = '') {
    $("#frm" + frm).validate();
    $("#frm" + frm).submit();
}
function checkFrm() {
    var fields = $("#frm").find("textarea, input").serializeArray();
    var result = true;
    $.each(fields, function (i, field) {
        if (typeof field.value !== undefined && field.value !== false && $('input[name=' + field.name + ']')[0].hasAttribute("required")) {
            $('input[name=' + field.name + ']')[0].focus();
            result = false;
        }
    });
    return result;
}