function get_wilayah(fromWilayah='kabupaten',toWilayah='kecamatan',callbackUrl='') {
    var id = $("#"+fromWilayah).val();
    console.log("FROM :"+fromWilayah);
    console.log("TO :"+ toWilayah);
    console.log("url : "+callbackUrl);
    $.ajax({
        type: "POST",
        url: baseURL+"wilayah",
        data: {'id':id,'wilayah':toWilayah,'ref_wilayah':fromWilayah},
        success: function (data) {
            $("#"+toWilayah).html(data);
        },
        error: function (XMLHttpRequest) {
            // alert(XMLHttpRequest.responseText);
            $("#error").html(XMLHttpRequest.responseText);
        }
    });
}

function get_kec(callbackUrl) {
    var id_kab = $("#kabkot").val();
    $.ajax({
        type: "POST",
        url: callbackUrl,
        data: "idkab=" + id_kab,
        success: function (data) {
            $("#kecamatan").html(data);
            get_desa();
        },
        error: function (XMLHttpRequest) {
            // alert(XMLHttpRequest.responseText);
            $("#error").html(XMLHttpRequest.responseText);
        }
    });
}

function get_desa(callbackUrl) {
    var id_kec = $("#kecamatan").val();
    $.ajax({
        type: "POST",
        url: callbackUrl,
        data: "id=" + id_kec,
        success: function (data) {
            $("#desa").html(data);
            CariAll();
        },
        error: function (XMLHttpRequest) {
            // alert(XMLHttpRequest.responseText);
            $("#error").html(XMLHttpRequest.responseText);
        }
    });
}