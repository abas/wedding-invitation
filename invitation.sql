-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: 17 Des 2019 pada 22.58
-- Versi Server: 5.7.28-0ubuntu0.18.04.4
-- PHP Version: 7.2.24-0ubuntu0.18.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `undangan`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `attendance`
--

CREATE TABLE `attendance` (
  `id` varchar(255) NOT NULL,
  `message` text,
  `attend` int(1) DEFAULT NULL,
  `invitation_id` int(11) NOT NULL,
  `guests_id` int(11) NOT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `author` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `galery`
--

CREATE TABLE `galery` (
  `id` int(11) NOT NULL,
  `title` varchar(45) DEFAULT NULL,
  `orig_name` varchar(255) DEFAULT NULL,
  `encrypt_name` varchar(255) DEFAULT NULL,
  `invitation_id` int(11) NOT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `author` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `guests`
--

CREATE TABLE `guests` (
  `id` int(11) NOT NULL,
  `full_name` varchar(255) DEFAULT NULL,
  `gender` enum('MALE','FAMALE') DEFAULT NULL,
  `degree` varchar(45) DEFAULT NULL,
  `phone_number` varchar(20) DEFAULT NULL,
  `address` text,
  `token` varchar(255) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `author` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `information`
--

CREATE TABLE `information` (
  `id` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `location` varchar(255) DEFAULT NULL,
  `date_information` date DEFAULT NULL,
  `time_information` time DEFAULT NULL,
  `invitation_id` int(11) NOT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `author` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `information`
--

INSERT INTO `information` (`id`, `name`, `location`, `date_information`, `time_information`, `invitation_id`, `created_date`, `updated_date`, `author`) VALUES
(1, 'Resepsi', 'Grand Sumber Ria Ballrom', '2020-10-13', '19:00:00', 1, '2019-12-17 00:00:00', '2019-12-17 00:00:00', 'Dian');

-- --------------------------------------------------------

--
-- Struktur dari tabel `invitation`
--

CREATE TABLE `invitation` (
  `id` int(11) NOT NULL,
  `title` varchar(100) DEFAULT NULL,
  `users_id` varchar(45) DEFAULT NULL,
  `siteurl` varchar(255) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `author` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `invitation`
--

INSERT INTO `invitation` (`id`, `title`, `users_id`, `siteurl`, `created_date`, `updated_date`, `author`) VALUES
(1, 'MEGI & DIAN', '1', 'megidianmengundang.com', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `invitation_detail`
--

CREATE TABLE `invitation_detail` (
  `id` int(11) NOT NULL,
  `full_name` varchar(255) DEFAULT NULL,
  `short_name` varchar(255) DEFAULT NULL,
  `address` varchar(100) DEFAULT NULL,
  `description` text,
  `bride_groom` tinyint(1) DEFAULT NULL,
  `picture` varchar(255) DEFAULT NULL,
  `invitation_id` int(11) NOT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `author` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `invitation_detail`
--

INSERT INTO `invitation_detail` (`id`, `full_name`, `short_name`, `address`, `description`, `bride_groom`, `picture`, `invitation_id`, `created_date`, `updated_date`, `author`) VALUES
(1, 'Dian Rosmala Sari', 'Dian', 'Jl.Bali', 'Putri pertama dari bapak Latip Kapiso & ibu Rosmiati Toliu', 0, '2e3f233bafe355dc7b68218617a6acf5.png', 1, NULL, '2019-12-17 22:13:57', 'dian'),
(2, 'Djafar Yusuf, SH', 'Megi', 'Suwawa', 'Putra tunggal dari bapak Harun Yusuf & Ibu Haliko Suli', 1, 'd614ce61720e362b4fd5988825e2f96a.png', 1, NULL, '2019-12-17 22:14:02', 'dian');

-- --------------------------------------------------------

--
-- Struktur dari tabel `level`
--

CREATE TABLE `level` (
  `id` int(11) NOT NULL,
  `nama` varchar(45) DEFAULT NULL,
  `keterangan` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `level`
--

INSERT INTO `level` (`id`, `nama`, `keterangan`) VALUES
(1, 'Administartor', 'Level Pimpinan');

-- --------------------------------------------------------

--
-- Struktur dari tabel `smart_audit`
--

CREATE TABLE `smart_audit` (
  `kode` varchar(255) NOT NULL,
  `audit_url` tinytext NOT NULL,
  `auditor` varchar(255) NOT NULL,
  `ip_address` varchar(50) NOT NULL,
  `audit_date` datetime NOT NULL,
  `audit_table` varchar(255) NOT NULL,
  `audit_action` varchar(50) NOT NULL,
  `audit_data` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `smart_audit`
--

INSERT INTO `smart_audit` (`kode`, `audit_url`, `auditor`, `ip_address`, `audit_date`, `audit_table`, `audit_action`, `audit_data`) VALUES
('00c92178-20d5-11ea-b0d0-28c2dd0cad73', 'http://localhost/undangan/apps/settings', 'dian', '::1', '2019-12-17 21:56:20', 'invitation_detail', 'UPDATE', 'UPDATE `invitation_detail` SET `full_name` = \'Dian Rosmala Sari\', `short_name` = \'Dian\', `address` = \'Jl.Bali\', `description` = \'Putri pertama dari bapak Latip Kapiso & ibu Rosmiati Toliu\', `picture` = \'bbd4507d80125733e7ba5541b546b6c7.gif\', `updated_date` = \'2019-12-17 21:56:20\', `author` = \'dian\''),
('01c0886f-20d3-11ea-b0d0-28c2dd0cad73', 'http://localhost/undangan/apps/settings', 'dian', '::1', '2019-12-17 21:42:02', 'invitation_detail', 'UPDATE', 'UPDATE `invitation_detail` SET `full_name` = \'Dian Rosmala Sari\', `short_name` = \'Dian\', `address` = \'Jl.Bali\', `description` = \'Putri pertama dari bapak Latip Kapiso & ibu Rosmiati Toliu\', `picture` = \'a3497abad34877a17126e2a37a37f7eb.png\', `updated_date` = \'2019-12-17 21:42:02\', `author` = \'dian\''),
('01ce202d-20d3-11ea-b0d0-28c2dd0cad73', 'http://localhost/undangan/apps/settings', 'dian', '::1', '2019-12-17 21:42:02', 'invitation_detail', 'UPDATE', 'UPDATE `invitation_detail` SET `full_name` = \'Djafar Yusuf, SH\', `short_name` = \'MEGI\', `address` = \'Suwawa\', `description` = \'Putra tunggal dari bapak Harun Yusuf & Ibu Haliko Suli\', `picture` = \'\', `updated_date` = \'2019-12-17 21:42:02\', `author` = \'dian\''),
('09f50c9d-20d3-11ea-b0d0-28c2dd0cad73', 'http://localhost/undangan/apps/settings', 'dian', '::1', '2019-12-17 21:42:16', 'invitation_detail', 'UPDATE', 'UPDATE `invitation_detail` SET `full_name` = \'Dian Rosmala Sari\', `short_name` = \'Dian\', `address` = \'Jl.Bali\', `description` = \'Putri pertama dari bapak Latip Kapiso & ibu Rosmiati Toliu\', `picture` = \'\', `updated_date` = \'2019-12-17 21:42:16\', `author` = \'dian\''),
('0a02a018-20d3-11ea-b0d0-28c2dd0cad73', 'http://localhost/undangan/apps/settings', 'dian', '::1', '2019-12-17 21:42:16', 'invitation_detail', 'UPDATE', 'UPDATE `invitation_detail` SET `full_name` = \'Djafar Yusuf, SH\', `short_name` = \'MEGI\', `address` = \'Suwawa\', `description` = \'Putra tunggal dari bapak Harun Yusuf & Ibu Haliko Suli\', `picture` = \'\', `updated_date` = \'2019-12-17 21:42:16\', `author` = \'dian\''),
('0a684fff-20d5-11ea-b0d0-28c2dd0cad73', 'http://localhost/undangan/apps/settings', 'dian', '::1', '2019-12-17 21:56:36', 'invitation_detail', 'UPDATE', 'UPDATE `invitation_detail` SET `full_name` = \'Djafar Yusuf, SH\', `short_name` = \'MEGI\', `address` = \'Suwawa\', `description` = \'Putra tunggal dari bapak Harun Yusuf & Ibu Haliko Suli\', `picture` = \'7bfffee4bb12e7feedd07697d5e08ede.gif\', `updated_date` = \'2019-12-17 21:56:36\', `author` = \'dian\''),
('101bbf8c-20d5-11ea-b0d0-28c2dd0cad73', 'http://localhost/undangan/apps/settings', 'dian', '::1', '2019-12-17 21:56:45', 'invitation_detail', 'UPDATE', 'UPDATE `invitation_detail` SET `full_name` = \'Dian Rosmala Sari\', `short_name` = \'Dian\', `address` = \'Jl.Bali\', `description` = \'Putri pertama dari bapak Latip Kapiso & ibu Rosmiati Toliu\', `picture` = \'f4ed6332d9d3c7734946dc28ac447cd7.gif\', `updated_date` = \'2019-12-17 21:56:45\', `author` = \'dian\''),
('277d018f-20d5-11ea-b0d0-28c2dd0cad73', 'http://localhost/undangan/apps/settings', 'dian', '::1', '2019-12-17 21:57:25', 'invitation_detail', 'UPDATE', 'UPDATE `invitation_detail` SET `full_name` = \'Dian Rosmala Sari\', `short_name` = \'Dian\', `address` = \'Jl.Bali\', `description` = \'Putri pertama dari bapak Latip Kapiso & ibu Rosmiati Toliu\', `picture` = \'71c5bc6dc38fe47774618484d26ec8cb.gif\', `updated_date` = \'2019-12-17 21:57:25\', `author` = \'dian\''),
('3613e1a4-20d4-11ea-b0d0-28c2dd0cad73', 'http://localhost/undangan/apps/settings', 'dian', '::1', '2019-12-17 21:50:40', 'invitation_detail', 'UPDATE', 'UPDATE `invitation_detail` SET `full_name` = \'Dian Rosmala Sari\', `short_name` = \'Dian\', `address` = \'Jl.Bali\', `description` = \'Putri pertama dari bapak Latip Kapiso & ibu Rosmiati Toliu\', `picture` = \'b417562cfb0c998c53303b0b9ad44e96.png\', `updated_date` = \'2019-12-17 21:50:40\', `author` = \'dian\''),
('38c96089-20d7-11ea-b0d0-28c2dd0cad73', 'http://localhost/undangan/apps/settings', 'dian', '::1', '2019-12-17 22:12:13', 'invitation_detail', 'UPDATE', 'UPDATE `invitation_detail` SET `full_name` = \'Dian Rosmala Sari\', `short_name` = \'Dian\', `address` = \'Jl.Bali\', `description` = \'Putri pertama dari bapak Latip Kapiso & ibu Rosmiati Toliu\', `picture` = \'4f0f9beb47b897bcefbe0c07766cd8b4.gif\', `updated_date` = \'2019-12-17 22:12:13\', `author` = \'dian\''),
('3b01dde9-20d4-11ea-b0d0-28c2dd0cad73', 'http://localhost/undangan/apps/settings', 'dian', '::1', '2019-12-17 21:50:48', 'invitation_detail', 'UPDATE', 'UPDATE `invitation_detail` SET `full_name` = \'Djafar Yusuf, SH\', `short_name` = \'MEGI\', `address` = \'Suwawa\', `description` = \'Putra tunggal dari bapak Harun Yusuf & Ibu Haliko Suli\', `picture` = \'\', `updated_date` = \'2019-12-17 21:50:48\', `author` = \'dian\''),
('42e9878d-20d7-11ea-b0d0-28c2dd0cad73', 'http://localhost/undangan/apps/settings', 'dian', '::1', '2019-12-17 22:12:30', 'invitation_detail', 'UPDATE', 'UPDATE `invitation_detail` SET `full_name` = \'Dian Rosmala Sari\', `short_name` = \'Dian\', `address` = \'Jl.Bali\', `description` = \'Putri pertama dari bapak Latip Kapiso & ibu Rosmiati Toliu\', `picture` = \'905f4165a9e5f6f19a1cb481c59db0b6.gif\', `updated_date` = \'2019-12-17 22:12:30\', `author` = \'dian\''),
('4859ea75-20d4-11ea-b0d0-28c2dd0cad73', 'http://localhost/undangan/apps/settings', 'dian', '::1', '2019-12-17 21:51:10', 'invitation_detail', 'UPDATE', 'UPDATE `invitation_detail` SET `full_name` = \'Djafar Yusuf, SH\', `short_name` = \'MEGI\', `address` = \'Suwawa\', `description` = \'Putra tunggal dari bapak Harun Yusuf & Ibu Haliko Suli\', `picture` = \'71c0c11302b33c1e2973e4b5946c256c.png\', `updated_date` = \'2019-12-17 21:51:10\', `author` = \'dian\''),
('49562cab-20d5-11ea-b0d0-28c2dd0cad73', 'http://localhost/undangan/apps/settings', 'dian', '::1', '2019-12-17 21:58:21', 'invitation_detail', 'UPDATE', 'UPDATE `invitation_detail` SET `full_name` = \'Dian Rosmala Sari\', `short_name` = \'Dian\', `address` = \'Jl.Bali\', `description` = \'Putri pertama dari bapak Latip Kapiso & ibu Rosmiati Toliu\', `picture` = \'50adaebc239ddb88397515746d60197e.gif\', `updated_date` = \'2019-12-17 21:58:21\', `author` = \'dian\''),
('5001abf8-20d7-11ea-b0d0-28c2dd0cad73', 'http://localhost/undangan/apps/settings', 'dian', '::1', '2019-12-17 22:12:52', 'invitation_detail', 'UPDATE', 'UPDATE `invitation_detail` SET `full_name` = \'Dian Rosmala Sari\', `short_name` = \'Dian\', `address` = \'Jl.Bali\', `description` = \'Putri pertama dari bapak Latip Kapiso & ibu Rosmiati Toliu\', `picture` = \'3af063b14cb05994a2fd5eb062206361.gif\', `updated_date` = \'2019-12-17 22:12:52\', `author` = \'dian\''),
('55e7035b-20d7-11ea-b0d0-28c2dd0cad73', 'http://localhost/undangan/apps/settings', 'dian', '::1', '2019-12-17 22:13:01', 'invitation_detail', 'UPDATE', 'UPDATE `invitation_detail` SET `full_name` = \'Dian Rosmala Sari\', `short_name` = \'Dian\', `address` = \'Jl.Bali\', `description` = \'Putri pertama dari bapak Latip Kapiso & ibu Rosmiati Toliu\', `picture` = \'2c2ba112d5742a82e001bcbeeec58bf5.gif\', `updated_date` = \'2019-12-17 22:13:01\', `author` = \'dian\''),
('57656441-20d4-11ea-b0d0-28c2dd0cad73', 'http://localhost/undangan/apps/settings', 'dian', '::1', '2019-12-17 21:51:35', 'invitation_detail', 'UPDATE', 'UPDATE `invitation_detail` SET `full_name` = \'Dian Rosmala Sari\', `short_name` = \'Dian\', `address` = \'Jl.Bali\', `description` = \'Putri pertama dari bapak Latip Kapiso & ibu Rosmiati Toliu\', `picture` = \'\', `updated_date` = \'2019-12-17 21:51:35\', `author` = \'dian\''),
('62dad2b8-20d7-11ea-b0d0-28c2dd0cad73', 'http://localhost/undangan/apps/settings', 'dian', '::1', '2019-12-17 22:13:23', 'invitation_detail', 'UPDATE', 'UPDATE `invitation_detail` SET `full_name` = NULL, `short_name` = NULL, `address` = NULL, `description` = NULL, `picture` = \'7bfffee4bb12e7feedd07697d5e08ede.gif\', `updated_date` = \'2019-12-17 22:13:23\', `author` = \'dian\''),
('6f565c71-20d7-11ea-b0d0-28c2dd0cad73', 'http://localhost/undangan/apps/settings', 'dian', '::1', '2019-12-17 22:13:44', 'invitation_detail', 'UPDATE', 'UPDATE `invitation_detail` SET `full_name` = \'Djafar Yusuf, SH\', `short_name` = \'Megi\', `address` = \'Suwawa\', `description` = \'Putra tunggal dari bapak Harun Yusuf & Ibu Haliko Suli\', `picture` = \'7bfffee4bb12e7feedd07697d5e08ede.gif\', `updated_date` = \'2019-12-17 22:13:44\', `author` = \'dian\''),
('76fcf219-20d7-11ea-b0d0-28c2dd0cad73', 'http://localhost/undangan/apps/settings', 'dian', '::1', '2019-12-17 22:13:57', 'invitation_detail', 'UPDATE', 'UPDATE `invitation_detail` SET `full_name` = \'Dian Rosmala Sari\', `short_name` = \'Dian\', `address` = \'Jl.Bali\', `description` = \'Putri pertama dari bapak Latip Kapiso & ibu Rosmiati Toliu\', `picture` = \'2e3f233bafe355dc7b68218617a6acf5.png\', `updated_date` = \'2019-12-17 22:13:57\', `author` = \'dian\''),
('7a27ea91-20d7-11ea-b0d0-28c2dd0cad73', 'http://localhost/undangan/apps/settings', 'dian', '::1', '2019-12-17 22:14:02', 'invitation_detail', 'UPDATE', 'UPDATE `invitation_detail` SET `full_name` = \'Djafar Yusuf, SH\', `short_name` = \'Megi\', `address` = \'Suwawa\', `description` = \'Putra tunggal dari bapak Harun Yusuf & Ibu Haliko Suli\', `picture` = \'d614ce61720e362b4fd5988825e2f96a.png\', `updated_date` = \'2019-12-17 22:14:02\', `author` = \'dian\''),
('9178a1d3-20d7-11ea-b0d0-28c2dd0cad73', 'http://localhost/undangan/apps/guests/new', 'dian', '::1', '2019-12-17 22:14:41', 'guests', 'INSERT', 'INSERT INTO `guests` (`id`, `full_name`, `gender`, `degree`, `phone_number`, `address`, `token`, `created_date`, `updated_date`, `author`) VALUES (\'1\', \'Abas Djumadi\', \'MALE\', \'Amd\', \'-\', \'Jl. Megawati, Kel. Tenilo, Kec. Limboto\', \'272bd2f9-0a42-5885-9084-fc15d8b4a478\', \'2019-12-17 22:14:41\', \'2019-12-17 22:14:41\', \'dian\')'),
('918f04db-20d7-11ea-b0d0-28c2dd0cad73', 'http://localhost/undangan/apps/guests/new', 'dian', '::1', '2019-12-17 22:14:42', 'sosmed', 'INSERT', 'INSERT INTO `sosmed` (`id`, `facebook`, `instagram`, `twitter`, `guests_id`, `created_date`, `updated_date`, `author`) VALUES (\'1\', \'abas.djumadi\', \'abas.djumadi\', \'AbasDjumadi\', \'1\', \'2019-12-17 22:14:41\', \'2019-12-17 22:14:41\', \'dian\')'),
('9a6cfc43-20dd-11ea-b0d0-28c2dd0cad73', 'http://localhost/undangan/apps/guests/1/delete', 'dian', '::1', '2019-12-17 22:57:53', 'sosmed', 'DELETE BY', 'DELETE FROM `sosmed`\nWHERE `guests_id` = \'1\''),
('9a8b949f-20dd-11ea-b0d0-28c2dd0cad73', 'http://localhost/undangan/apps/guests/1/delete', 'dian', '::1', '2019-12-17 22:57:54', 'guests', 'DELETE', 'DELETE FROM `guests`\nWHERE `id` = \'1\''),
('aace6cd8-20d6-11ea-b0d0-28c2dd0cad73', 'http://localhost/undangan/apps/settings', 'dian', '::1', '2019-12-17 22:08:14', 'invitation_detail', 'UPDATE', 'UPDATE `invitation_detail` SET `full_name` = \'Dian Rosmala Sari\', `short_name` = \'Dian\', `address` = \'Jl.Bali\', `description` = \'Putri pertama dari bapak Latip Kapiso & ibu Rosmiati Toliu\', `picture` = \'192d261ecd6e1b47cd626ba2fe508584.gif\', `updated_date` = \'2019-12-17 22:08:14\', `author` = \'dian\''),
('bce5f177-20d4-11ea-b0d0-28c2dd0cad73', 'http://localhost/undangan/apps/settings', 'dian', '::1', '2019-12-17 21:54:26', 'invitation_detail', 'UPDATE', 'UPDATE `invitation_detail` SET `full_name` = \'Dian Rosmala Sari\', `short_name` = \'Dian\', `address` = \'Jl.Bali\', `description` = \'Putri pertama dari bapak Latip Kapiso & ibu Rosmiati Toliu\', `picture` = \'ea1b76106057caef7093ef97f250e111.png\', `updated_date` = \'2019-12-17 21:54:26\', `author` = \'dian\''),
('c10077f1-20d4-11ea-b0d0-28c2dd0cad73', 'http://localhost/undangan/apps/settings', 'dian', '::1', '2019-12-17 21:54:33', 'invitation_detail', 'UPDATE', 'UPDATE `invitation_detail` SET `full_name` = \'Djafar Yusuf, SH\', `short_name` = \'MEGI\', `address` = \'Suwawa\', `description` = \'Putra tunggal dari bapak Harun Yusuf & Ibu Haliko Suli\', `picture` = \'0d5e6de836eddb08cc4928aaaa964acc.png\', `updated_date` = \'2019-12-17 21:54:33\', `author` = \'dian\''),
('c2b2caf6-20d4-11ea-b0d0-28c2dd0cad73', 'http://localhost/undangan/apps/settings', 'dian', '::1', '2019-12-17 21:54:35', 'invitation_detail', 'UPDATE', 'UPDATE `invitation_detail` SET `full_name` = \'Djafar Yusuf, SH\', `short_name` = \'MEGI\', `address` = \'Suwawa\', `description` = \'Putra tunggal dari bapak Harun Yusuf & Ibu Haliko Suli\', `picture` = \'0d5e6de836eddb08cc4928aaaa964acc.png\', `updated_date` = \'2019-12-17 21:54:35\', `author` = \'dian\''),
('ca78508c-20d4-11ea-b0d0-28c2dd0cad73', 'http://localhost/undangan/apps/settings', 'dian', '::1', '2019-12-17 21:54:49', 'invitation_detail', 'UPDATE', 'UPDATE `invitation_detail` SET `full_name` = \'Dian Rosmala Sari\', `short_name` = \'Dian\', `address` = \'Jl.Bali\', `description` = \'Putri pertama dari bapak Latip Kapiso & ibu Rosmiati Toliu\', `picture` = \'ea1b76106057caef7093ef97f250e111.png\', `updated_date` = \'2019-12-17 21:54:48\', `author` = \'dian\''),
('f603efa2-20d4-11ea-b0d0-28c2dd0cad73', 'http://localhost/undangan/apps/settings', 'dian', '::1', '2019-12-17 21:56:02', 'invitation_detail', 'UPDATE', 'UPDATE `invitation_detail` SET `full_name` = \'Dian Rosmala Sari\', `short_name` = \'Dian\', `address` = \'Jl.Bali\', `description` = \'Putri pertama dari bapak Latip Kapiso & ibu Rosmiati Toliu\', `picture` = \'7193e218e1344b66d498b44f233b93e3.png\', `updated_date` = \'2019-12-17 21:56:02\', `author` = \'dian\''),
('f7a1ea52-20d4-11ea-b0d0-28c2dd0cad73', 'http://localhost/undangan/apps/settings', 'dian', '::1', '2019-12-17 21:56:04', 'invitation_detail', 'UPDATE', 'UPDATE `invitation_detail` SET `full_name` = \'Dian Rosmala Sari\', `short_name` = \'Dian\', `address` = \'Jl.Bali\', `description` = \'Putri pertama dari bapak Latip Kapiso & ibu Rosmiati Toliu\', `picture` = \'7193e218e1344b66d498b44f233b93e3.png\', `updated_date` = \'2019-12-17 21:56:04\', `author` = \'dian\''),
('fbc1723b-20d4-11ea-b0d0-28c2dd0cad73', 'http://localhost/undangan/apps/settings', 'dian', '::1', '2019-12-17 21:56:11', 'invitation_detail', 'UPDATE', 'UPDATE `invitation_detail` SET `full_name` = \'Dian Rosmala Sari\', `short_name` = \'Dian\', `address` = \'Jl.Bali\', `description` = \'Putri pertama dari bapak Latip Kapiso & ibu Rosmiati Toliu\', `picture` = \'48c27655f63ff7437ed16eab7b5edb3b.png\', `updated_date` = \'2019-12-17 21:56:11\', `author` = \'dian\'');

-- --------------------------------------------------------

--
-- Struktur dari tabel `sosmed`
--

CREATE TABLE `sosmed` (
  `id` int(11) NOT NULL,
  `facebook` varchar(45) DEFAULT NULL,
  `instagram` varchar(45) DEFAULT NULL,
  `twitter` varchar(45) DEFAULT NULL,
  `guests_id` int(11) NOT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `author` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(45) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `nice_name` varchar(45) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `level_id` int(11) DEFAULT NULL,
  `pin_aktivasi` varchar(10) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `author` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id`, `username`, `email`, `password`, `nice_name`, `status`, `level_id`, `pin_aktivasi`, `created_date`, `updated_date`, `author`) VALUES
(1, 'dian', 'dianrosmala@gmail.com', '7a6443a9aba9cb3e7ca95e6902abbec6', 'Dian', 1, 1, 'istimewa', '2019-12-16 22:26:10', '2019-12-16 22:26:10', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `user_session`
--

CREATE TABLE `user_session` (
  `id` varchar(128) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `data` blob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_attendance`
-- (Lihat di bawah untuk tampilan aktual)
--
CREATE TABLE `v_attendance` (
`id` varchar(255)
,`message` text
,`attend` int(1)
,`invitation_id` int(11)
,`guests_id` int(11)
,`full_name` varchar(255)
);

-- --------------------------------------------------------

--
-- Struktur untuk view `v_attendance`
--
DROP TABLE IF EXISTS `v_attendance`;

CREATE ALGORITHM=UNDEFINED DEFINER=`admin`@`localhost` SQL SECURITY DEFINER VIEW `v_attendance`  AS  select `a`.`id` AS `id`,`a`.`message` AS `message`,`a`.`attend` AS `attend`,`a`.`invitation_id` AS `invitation_id`,`a`.`guests_id` AS `guests_id`,`b`.`full_name` AS `full_name` from (`attendance` `a` left join `guests` `b` on((`a`.`guests_id` = `b`.`id`))) ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `attendance`
--
ALTER TABLE `attendance`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_attendance_invitation1_idx` (`invitation_id`),
  ADD KEY `fk_attendance_guests1_idx` (`guests_id`);

--
-- Indexes for table `galery`
--
ALTER TABLE `galery`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_galery_invitation1_idx` (`invitation_id`);

--
-- Indexes for table `guests`
--
ALTER TABLE `guests`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `information`
--
ALTER TABLE `information`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_information_invitation1_idx` (`invitation_id`);

--
-- Indexes for table `invitation`
--
ALTER TABLE `invitation`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `invitation_detail`
--
ALTER TABLE `invitation_detail`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_invitation_detail_invitation1_idx` (`invitation_id`);

--
-- Indexes for table `level`
--
ALTER TABLE `level`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `smart_audit`
--
ALTER TABLE `smart_audit`
  ADD PRIMARY KEY (`kode`);

--
-- Indexes for table `sosmed`
--
ALTER TABLE `sosmed`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_sosmed_guests1_idx` (`guests_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_users_1_idx` (`level_id`);

--
-- Indexes for table `user_session`
--
ALTER TABLE `user_session`
  ADD KEY `user_session_timestamp` (`timestamp`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `level`
--
ALTER TABLE `level`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `attendance`
--
ALTER TABLE `attendance`
  ADD CONSTRAINT `fk_attendance_guests1` FOREIGN KEY (`guests_id`) REFERENCES `guests` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_attendance_invitation1` FOREIGN KEY (`invitation_id`) REFERENCES `invitation` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ketidakleluasaan untuk tabel `galery`
--
ALTER TABLE `galery`
  ADD CONSTRAINT `fk_galery_invitation1` FOREIGN KEY (`invitation_id`) REFERENCES `invitation` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ketidakleluasaan untuk tabel `information`
--
ALTER TABLE `information`
  ADD CONSTRAINT `fk_information_invitation1` FOREIGN KEY (`invitation_id`) REFERENCES `invitation` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ketidakleluasaan untuk tabel `invitation_detail`
--
ALTER TABLE `invitation_detail`
  ADD CONSTRAINT `fk_invitation_detail_invitation1` FOREIGN KEY (`invitation_id`) REFERENCES `invitation` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ketidakleluasaan untuk tabel `sosmed`
--
ALTER TABLE `sosmed`
  ADD CONSTRAINT `fk_sosmed_guests1` FOREIGN KEY (`guests_id`) REFERENCES `guests` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ketidakleluasaan untuk tabel `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `fk_users_1` FOREIGN KEY (`level_id`) REFERENCES `level` (`id`) ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
