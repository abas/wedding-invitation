<nav class="navbar navbar-default" role="navigation">
    <div class="container">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
          <span class="sr-only">Toggle nav</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>

        <!-- Logo text or image -->
        <a class="navbar-brand" href="<?= site_url().'?a='.$this->input->get('a') ?>"><?= $groom->short_name ?> & <?= $bride->short_name ?></a>

      </div>
      <div class="navigation collapse navbar-collapse navbar-ex1-collapse">
        <ul class="nav navbar-nav">
          <li class="current"><a href="#intro">WEDDING</a></li>
          <li><a href="#about">INFORMATION</a></li>
          <li><a href="#services">RECEPTION</a></li>
          <li><a href="#portfolio">GALLERY</a></li>
          <li><a href="#contact">Attendance</a></li>
        </ul>
      </div>
    </div>
  </nav>
