<!-- intro area -->
<section id="intro">
    <div class="intro-container">
      <div id="introCarousel" class="carousel slide carousel-fade" data-ride="carousel">

        <div class="carousel-inner" role="listbox">

          <!-- Slide 1 -->
          <div class="item active">
            <div class="carousel-background"><img src="<?= base_url('uploads/images/2.jpg') ?>" alt=""></div>
            <div class="carousel-container">
              <div class="carousel-content">
                <h2 class="animated fadeInDown dianStyleFontLL">Together with their families</h2>
                <h2 class="animated fadeInDown dianStyleFont"><?= ucfirst($groom->short_name) ?> <span>&</span> <?= ucfirst($bride->short_name) ?></h2>
                <?php if($kepada != ''): ?>
                  <h5 class="animated fadeInUp">Request the honor of your</h5>
                  <h3 class="animated fadeInUp">precense</h3>
                  <h4 class="animated fadeInUp mb-0"><?= $kepada; ?></h4>
                <p class="animated fadeInUp dianStyleWR">to their wedding reception
                </p>
                <?php endif; ?>
                <a href="#about" class="btn-get-started animated fadeInUp bg-green">LIHAT UNDANGAN</a>
              </div>
            </div>
          </div>
          
          <!-- Slide 2 -->
          <div class="item">
            <div class="carousel-background"><img src="<?= base_url('uploads/images/1.jpg') ?>" alt=""></div>
            <div class="carousel-container">
              <div class="carousel-content">
                <h2 class="animated fadeInDown dianStyleFont"><?= ucfirst($groom->short_name) ?> <span>&</span> <?= ucfirst($bride->short_name) ?></h2>
                <div class="text-center">
                  
                </div>
                <a href="#about" class="btn-get-started animated fadeInUp bg-green">Lihat Undangan</a>
              </div>
            </div>
          </div>

          <!-- Slide 3 -->
          <div class="item">
            <div class="carousel-background"><img src="<?= base_url('uploads/images/3.JPG') ?>" alt=""></div>
            <div class="carousel-container">
              <div class="carousel-content">
                <h2 class="animated fadeInDown dianStyleFont"><?= ucfirst($groom->short_name) ?> <span>&</span> <?= ucfirst($bride->short_name) ?></h2>
                <p class="animated fadeInUp">Reception
                  <br/> <i class="fa fa-map-marker"></i> Grand Sumber Ria Ballrom
                  <br/> <i class="fa fa-calendar"></i> Senin 13 Januari 2020
                  <br/> <i class="fa fa-clock-o"></i> 19.00 Wita
                </p>
                <a href="#about" class="btn-get-started animated fadeInUp bg-green">Lihat Undangan</a>
              </div>
            </div>
          </div>

        </div>

        <a class="carousel-control-prev" href="#introCarousel" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon fa fa-angle-left" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>

        <a class="carousel-control-next" href="#introCarousel" role="button" data-slide="next">
          <span class="carousel-control-next-icon fa fa-angle-right" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>

      </div>
    </div>
  </section><!-- #intro -->

  <!-- About -->
  <section id="about" class="home-section bg-white">
    <div class="container">
      <div class="row">
        <div class="col-md-offset-2 col-md-8">
          <div class="section-heading">
            <h2 class="dianStyleFontH2">Bride & Groom</h2>
            <div class="heading-line"></div>
            <p>Informasi Mempelai Pria & Wanita</p>
          </div>
        </div>
      </div>
      <div class="row wow fadeInUp">
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
          <div class="box-team wow bounceInUp" data-wow-delay="0.1s" style="visibility: visible; animation-delay: 0.1s; animation-name: bounceInUp;">
            <img src="<?= (is_file('./uploads/images/'.$groom->picture)) ? base_url('uploads/images/'.$groom->picture) : 'https://scontent.fupg1-1.fna.fbcdn.net/v/t1.0-9/54197773_390680041664064_1766616436500856832_n.jpg?_nc_cat=111&_nc_eui2=AeEHsp1JzEWNIficQQgpFu41sImx15WqI0GOw6_owzzFILO68X7b2Uam6EpdsSDKXbJJ-GOTgsDCY_xjogPdcjjaPx7tm_QPJjHoaVbKkklAEA&_nc_oc=AQlc5bKVxbvgi6T0MahZZM6ucebQ7_dC_UYQekAkOzqq35-LA9vpMflNMi6WbIkvVuc&_nc_ht=scontent.fupg1-1.fna&oh=204d1dfeb5e559f8d818ee32cb26555a&oe=5EAF9B7B' ?>" alt="<?= $groom->full_name ?>" class="img-circle img-responsive">
            <h4><?= $groom->full_name ?> (<?= $groom->short_name ?>)</h4>
            <p><?= $groom->description ?></p>
          </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
          <div class="box-team wow bounceInUp" data-wow-delay="0.1s" style="visibility: visible; animation-delay: 0.1s; animation-name: bounceInUp;">
            <img src="<?= (is_file('./uploads/images/'.$bride->picture)) ? base_url('uploads/images/'.$bride->picture) : 'https://scontent.fupg1-1.fna.fbcdn.net/v/t1.0-9/54197773_390680041664064_1766616436500856832_n.jpg?_nc_cat=111&_nc_eui2=AeEHsp1JzEWNIficQQgpFu41sImx15WqI0GOw6_owzzFILO68X7b2Uam6EpdsSDKXbJJ-GOTgsDCY_xjogPdcjjaPx7tm_QPJjHoaVbKkklAEA&_nc_oc=AQlc5bKVxbvgi6T0MahZZM6ucebQ7_dC_UYQekAkOzqq35-LA9vpMflNMi6WbIkvVuc&_nc_ht=scontent.fupg1-1.fna&oh=204d1dfeb5e559f8d818ee32cb26555a&oe=5EAF9B7B' ?>" alt="<?= $bride->full_name ?>" class="img-circle img-responsive">
            <h4><?= $bride->full_name ?> (<?= $bride->short_name ?>)</h4>
            <p><?= $bride->description ?></p>
          </div>
        </div>
      </div>
    </div>
  </section>

   <!-- Parallax 2 -->
   <section id="parallax2" class="home-section parallax" data-stellar-background-ratio="0.5">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="color-light">
            <p class="wow bounceInUp" data-wow-delay="1s">"Aku adalah jari yang gemar mencatat angka dan rahasia. 
              <br/>Kamu adalah buku yang bisa menampung  semua rasa dan gerak yang dimiliki manusia. 
              <br/>Kita bertemu di Kitab Tuhan Yang Maha Bercerita. --menuju kisah dengan banyak kata kerja."</p>
          </div>
        </div>
      </div>
    </div>
  </section>

  <!-- Services -->
  <section id="services" class="home-section bg-gray">
    <div class="container">
      <div class="row">
        <div class="col-md-offset-2 col-md-8">
          <div class="section-heading">
            <h2 class="dianStyleFontH2">Wedding Reception</h2>
            <div class="heading-line"></div>
            <p>Resepsi pernikahan</p>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div id="carousel-service" class="service carousel slide">

            <!-- slides -->
            <div class="carousel-inner">
              <div class="item active">
                <div class="row">
                  <div class="col-sm-12 col-md-offset-1 col-md-6">
                    <div class="wow bounceInLeft">
                      <h4>Lokasi</h4>
                      <p><i class="fa fa-map-marker"></i> Grand Sumber Ria Ballrom</p>
                      <p><i class="fa fa-calendar"></i> Senin 13 Januari 2020</p>
                      <p><i class="fa fa-clock-o"></i> 19.00 Wita</p>
                      <div title="Add to Calendar" class="addeventatc" style="background:#2da89b; margin-bottom:20px;">
                          Add to Calendar
                          <span class="start">12/29/2019 08:00 AM</span>
                          <span class="end">12/29/2019 10:00 AM</span>
                          <span class="timezone">America/Los_Angeles</span>
                          <span class="title">Summary of the event</span>
                          <span class="description">Description of the event</span>
                          <span class="location">Location of the event</span>
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-12 col-md-5">
                    <div class="screenshot wow bounceInRight">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3989.6270778840744!2d123.06698421430504!3d0.5609897995907299!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x32792b50428fdb33%3A0x909de63644ffb01!2sGrand%20Sumber%20Ria%20Ballroom!5e0!3m2!1sid!2sid!4v1576595986760!5m2!1sid!2sid" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <!-- Works -->
  <section id="portfolio" class="home-section bg-white">
    <div class="container">
      <div class="row">
        <div class="col-md-offset-2 col-md-8">
          <div class="section-heading">
            <h2 class="dianStyleFontH2">Gallery</h2>
            <div class="heading-line"></div>
            <p>Foto Prewedding</p>
          </div>
        </div>
      </div>
      <?php if(!empty($galery)): ?>
        <div class="row">
              <?php 
                foreach ($galery as $key => $val) { ?>
                  <div class="col-12 col-md-4 col-sm-6">
                  <a class="thumbnail" href="#" data-image-id="<?= $val->id ?>" data-toggle="modal" data-title=""
                    data-image="<?= base_url('/uploads/images/'.$val->encrypt_name) ?>"
                    data-target="#image-gallery">
                      <img class="img-thumbnail"
                          src="<?= base_url('/uploads/images/'.$val->encrypt_name) ?>"
                          alt="<?= $val->title; ?>t">
                  </a>
                  </div>
                <?php }
              ?>
        </div>
      <?php endif; ?>
    </div>
  </section>

  <!-- Contact -->
  <section id="contact" class="home-section bg-gray">
    <div class="container">
      <div class="row">
        <div class="col-md-offset-2 col-md-8">
          <div class="section-heading">
            <h2 class="dianStyleFontH2">Attendance <br/>&<br/> Greeting</h2>
            <div class="heading-line"></div>
            <p>Ucapan & Konfirmasi Kehadiran </p>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-md-offset-2 col-md-8">
          <form action="<?= site_url('send/attendance?a='.$this->input->get('a')) ?>" method="post" class="form-horizontal" role="form">
            <div class="col-md-offset-2 col-md-8">
              <input type="hidden" name="guest" value="<?= $guest_id; ?>">
              <div class="form-group">
                <textarea class="form-control" name="message" rows="5" data-rule="required" data-msg="Please write something for us"
                  placeholder="Nasihat / Doa untuk kami"></textarea>
                <div class="validation"></div>
              </div>
              <div class="custom-control custom-radio custom-control-inline">
                <input type="radio" id="attend" name="attend" class="custom-control-input" value="0" checked>
                <label class="custom-control-label" for="attend">Saya akan datang</label>
              </div>
              <div class="custom-control custom-radio custom-control-inline">
                <input type="radio" id="customRadioInline2" name="attend" class="custom-control-input" value="1">
                <label class="custom-control-label" for="customRadioInline2">Saya mungkin akan datang</label>
              </div>
              <div class="custom-control custom-radio custom-control-inline">
                <input type="radio" id="customRadioInline2" name="attend" class="custom-control-input" value="2">
                <label class="custom-control-label" for="customRadioInline2">Saya tidak akan datang</label>
              </div>
            </div>
            <div class="form-group">
              <div class="col-md-offset-2 col-md-8">
                <button type="submit" class="btn btn-theme btn-lg btn-block">Kirim</button>
              </div>
            </div>
          </form>

        </div>
      </div>

    </div>
  </section>

    <!-- Parallax 1 -->
  <section id="parallax1" class="home-section parallax" data-stellar-background-ratio="0.5">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="color-light">
            <h2 class="wow bounceInDown bg-green" data-wow-delay="0.5s">Kami yang berbahagia</h2>
            <p class="lead wow bounceInUp" data-wow-delay="1s">Keluarga Yusuf
              <br/>Keluarga Suli
              <br/>Keluarga Kapiso
              <br/>Keluarga Toliu
              </p>
              <h2 class="animated fadeInDown dianStyleFont bg-green"><?= ucfirst($groom->short_name) ?> <span>&</span> <?= ucfirst($bride->short_name) ?></h2>
          </div>
        </div>
      </div>
    </div>
  </section>