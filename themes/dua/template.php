<!DOCTYPE html>
<html>

<head>
  <title><?= $title; ?></title>
  <meta charset="utf-8" />
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <?php include 'style.php'; ?>
  <style>
    ::placeholder { /* Chrome, Firefox, Opera, Safari 10.1+ */
    color: #FBCE3A;
    opacity: 1; /* Firefox */
    }
    
    :-ms-input-placeholder { /* Internet Explorer 10-11 */
    color: #FBCE3A;
    }
    
    ::-ms-input-placeholder { /* Microsoft Edge */
    color: #FBCE3A;
    }
  </style>
<?php include 'header.php'; ?>
</head>
<body>
<?php include 'content.php'; ?>
<?php include 'footer.php'; ?>
