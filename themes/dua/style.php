<!-- css -->
<link href="https://fonts.googleapis.com/css?family=Playfair+Display:400,400i,700,700i,900,900i&display=swap" rel="stylesheet">
<link href="<?= $theme_path ?>/css/bootstrap.min.css" rel="stylesheet" media="screen">
<link href="<?= $theme_path ?>/css/style.css" rel="stylesheet" media="screen">
<link href="<?= $theme_path ?>/color/default.css" rel="stylesheet" media="screen">
<style type="text/css">
    .btn:focus, .btn:active, button:focus, button:active {
    outline: none !important;
    box-shadow: none !important;
    }

    #image-gallery .modal-footer{
    display: block;
    }

    .thumb{
    margin-top: 15px;
    margin-bottom: 15px;
    }
    .img-thumbnail{
        padding: 0px!important;
        border: 0px!important;

    }
    .modal.fade {
        z-index: 10000000 !important;
    }
</style>